#!/usr/bin/env python3

# Read in a polyhedron description, and output a JavaScript list
# suitable for use in the "data" field of a canvas used by
# canvaspoly.js.

import sys
import os
import string
import random
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Convert a polyhedron into a canvaspoly.js data declaration.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
parser.add_argument("--html", action="store_true",
                    help="Write a test HTML file instead of just the data.")
parser.add_argument("-W", "--width", type=int, default=400,
                    help="In --html mode, specify width of canvas.")
parser.add_argument("-H", "--height", type=int, default=400,
                    help="In --html mode, specify height of canvas.")
args = parser.parse_args()

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, normals = poly.get_dicts()

# Normalise the radius of our polyhedron so that it just fits
# inside the unit sphere.
maxlen = 0
for v in vertices.values():
    vlen = sqrt(v[0]**2 + v[1]**2 + v[2]**2)
    if maxlen < vlen: maxlen = vlen
for v in vertices.keys():
    vv = vertices[v]
    vertices[v] = (vv[0]/maxlen, vv[1]/maxlen, vv[2]/maxlen)

# Assign integer indices to vertices and faces.
vindex = {}
findex = {}
vlist = []
flist = []
for v in vertices.keys():
    vindex[v] = len(vlist)
    vlist.append(v)
for f in faces.keys():
    findex[f] = len(flist)
    flist.append(f)

s = "[{:d}".format(len(vertices))
for i in range(len(vertices)):
    v = vertices[vlist[i]]
    s += ",{:.17g},{:.17g},{:.17g}".format(v[0], v[1], v[2])
s += ",{:d}".format(len(faces))
for i in range(len(faces)):
    f = faces[flist[i]]
    n = normals[flist[i]]
    s += ",{:d}".format(len(f))
    for v in f:
        s += ",{:d}".format(vindex[v])
    s += ",{:.17g},{:.17g},{:.17g}".format(n[0], n[1], n[2])
s += "]"

outfile = args.outfile()
if not args.html:
    outfile.write(s + "\n")
else:
    outfile.write("""\
<html>
<head>
<title>Canvas polyhedron visualisation</title>
<script type="text/javascript" src="canvaspoly.js"></script>
</head>
<body onload="initCanvasPolyhedra();">
<p>
<span class="polyhedron" width="{w:d}" height="{h:d}" data-polyhedron="{d}">
Alt text.
</span>
</body>
</html>
""".format(d=s, w=args.width, h=args.height))
outfile.close()
