import sys
import math
import collections
import itertools
from functools import reduce

def vadd(v, w):
    assert len(v) == len(w)
    return tuple(vx + wx for vx,wx in zip(v,w))

def vsub(v, w):
    assert len(v) == len(w)
    return tuple(vx - wx for vx,wx in zip(v,w))

def vscale(v, s):
    return tuple(vx * s for vx in v)

def vneg(v):
    return tuple(-vx for vx in v)

def vdot(v, w):
    assert len(v) == len(w)
    return sum(vx * wx for vx,wx in zip(v,w))

def vnorm2(v):
    return vdot(v, v)

def vnorm(v):
    return math.sqrt(vnorm2(v))

def vunit(v):
    return vscale(v, 1.0 / vnorm(v))

def vzero(dimensions=3):
    return (0.0,) * dimensions

def vcross(v, w):
    assert len(v) == len(w) == 3
    a, b, c = v
    x, y, z = w
    return (b*z-c*y, c*x-a*z, a*y-b*x)

def vrot90(v):
    x, y = v
    return y, -x

def normals_to(v):
    "Yield mutually orthogonal vectors of arbitrary length, orthogonal to v."

    i = next(i for i,x in enumerate(v) if x != 0)
    toret = []
    other_indices = [j for j in range(len(v)) if j != i]
    for ji, j in enumerate(other_indices):
        w = [0] * len(v)
        for k in [i] + other_indices[:ji]:
            w[k] = v[k] * v[j]
            w[j] -= v[k]**2
        yield w

def unit_normals_to(v):
    "Yield mutually orthogonal unit vectors, orthogonal to v."
    return (vunit(w) for w in normals_to(v))

class Matrix(object):
    def __init__(self, w, h, cols):
        cols = list(cols)
        assert len(cols) == w and all(len(col)==h for col in cols)
        self.w, self.h, self.cols = w, h, cols

    def columns(self):
        return [col[:] for col in self.cols]
    def rows(self):
        return self.transpose().columns()

    @classmethod
    def fromcols(cls, *cols):
        return cls(len(cols[0]), len(cols), cols)
    @classmethod
    def fromrows(cls, *rows):
        return cls.fromcols(*rows).transpose()
    @classmethod
    def fromcolsflat(cls, w, h, array):
        assert len(array) == w * h
        return cls.fromcols(*[array[s:s+h] for s in range(0, w*h, h)])
    @classmethod
    def fromrowsflat(cls, w, h, array):
        return cls.fromcolsflat(h, w, array).transpose()
    @classmethod
    def diagonal(cls, *diag):
        n = len(diag)
        return cls(n, n, [[0.0]*i + [d] + [0.0]*(n-i-1)
                          for i,d in enumerate(diag)])

    def square(self):
        return self.h == self.w
    def transpose(self):
        return Matrix(self.h, self.w, [[col[i] for col in self.cols]
                                       for i in range(self.h)])
    def apply(self, vector):
        assert len(vector) == self.w
        return tuple(sum(v * col[j] for v, col in zip(vector, self.cols))
                     for j in range(self.h))
    def mul(self, rhs):
        assert self.w == rhs.h
        return Matrix(self.h, rhs.w,
                      [[sum(col[y]*r for col,r in zip(self.cols, rcol))
                        for y in range(self.h)] for rcol in rhs.cols])
    def determinant_subset(self, colindices, rowindices, sign=+1):
        assert len(colindices) == len(rowindices)
        if len(colindices) == 1:
            return sign * self.cols[colindices[0]][rowindices[0]]
        ci_iter = iter(colindices)
        col = self.cols[next(ci_iter)]
        cis = list(ci_iter)
        toret = 0
        for ri in rowindices:
            toret += col[ri] * self.determinant_subset(
                cis, [i for i in rowindices if i != ri], sign)
            sign = -sign
        return toret
    def determinant(self):
        return self.determinant_subset(range(self.w), range(self.h), +1)

    @classmethod
    def rotate_to_z(cls, vector):
        """Make an orthogonal matrix with determinant 1 which rotates v to be
        parallel to the last basis vector, with positive sense."""

        assert len(vector) == 3
        cols = list(unit_normals_to(vector)) + [vunit(vector)]
        m = cls.fromrows(*cols)
        if m.determinant() < 0:
            cols[0] = vneg(cols[0])
            m = cls.fromrows(*cols)
        return m

    @classmethod
    def rotate_about_z(cls, angle):
        "Make a 3x3 matrix that rotates by 'angle' about the z-axis."
        c, s = math.cos(angle), math.sin(angle)
        return cls.fromcols([c, s, 0], [-s, c, 0], [0, 0, 1])

    @classmethod
    def rotate_about(cls, vector, angle):
        "Make a 3x3 matrix that rotates by 'angle' about 'vector'."
        assert len(vector) == 3
        m = cls.rotate_to_z(vector)
        r = cls.rotate_about_z(angle)
        return m.transpose().mul(r.mul(m))

class PointWriter(object):
    def __init__(self, fh, dimensions=3):
        self.fh = fh
        self.dimensions = dimensions
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.fh.close()
    def write(self, pt):
        assert len(pt) == self.dimensions
        print(*pt, file=self.fh)

class FileReader(object):
    def __init__(self, fh):
        self.fh = fh
        self.lineno = 0
    def __enter__(self):
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.fh.close()
    def warning(self, msg):
        try:
            msg = "{}:{:d}: {}\n".format(self.fh.name, self.lineno, msg)
        except NameError:
            msg = "line {:d}: {}\n".format(self.lineno, msg)
        sys.stderr.write(msg)
    def readline(self):
        line = self.fh.readline()
        if line == "":
            raise StopIteration
        self.lineno += 1
        return line.rstrip("\r\n")

class PointReader(FileReader):
    def __init__(self, fh, dimensions=3):
        super(PointReader, self).__init__(fh)
        self.dimensions = dimensions
    def __iter__(self):
        return self
    def next(self): return self.__next__()
    def __next__(self):
        while True:
            line = self.readline()
            words = line.split(' ')

            if len(words) == 0:
                continue

            if len(words) != self.dimensions:
                self.warning("expected {:d} fields per line, got {:d}"
                             .format(self.lineno, self.dimensions, len(words)))
                continue

            try:
                return tuple(float(word) for word in words)
            except ValueError:
                self.warning("line {:d}: could not parse all fields "
                                 "as floats\n".format(self.lineno))
                continue

def adjacent_tuples(iterable, n=2):
    it = iter(iterable)
    initial = [next(it) for _ in range(n-1)]
    prev = initial[:]
    for curr in itertools.chain(it, initial):
        prev.append(curr)
        yield tuple(prev)
        prev.pop(0)

class Polyhedron(object):
    def __init__(self):
        self.vertices = collections.OrderedDict()
        self.faces = collections.OrderedDict()
        self.normal_cache = {}
        self.vertexnames = ("vertex{:d}".format(i) for i in itertools.count())
        self.facenames = ("face{:d}".format(i) for i in itertools.count())

    def vertex(self, *coords, **kws):
        try:
            name = kws["name"]
        except KeyError:
            name = next(self.vertexnames)
        assert name not in self.vertices
        self.vertices[name] = tuple(coords)
        return name

    def face(self, *vertices, **kws):
        try:
            name = kws["name"]
        except KeyError:
            name = next(self.facenames)
        assert name not in self.faces
        self.faces[name] = []
        for v in vertices:
            self.add_to_face(name, v)
        return name

    def add_to_face(self, face, vertex):
        self.faces[face].append(vertex)
        if face in self.normal_cache:
            del self.normal_cache[face]

    def normal(self, face):
        if face not in self.normal_cache:
            # Compute the surface normal. To do this I'm going to take
            # the vector product of each adjacent pair of edges; _all_
            # of these should produce some non-zero vector which is
            # normal to the face. Then I'm simply going to sum those
            # to get a single canonical surface normal, normalise it
            # to unit length, and output it.

            vs = [self.vertices[v] for v in self.faces[face]]

            normal = vzero()
            for u, v, w in adjacent_tuples(vs, 3):
                normal = vadd(normal, vcross(vsub(v,u), vsub(w,v)))

            self.normal_cache[face] = vunit(normal)
        return self.normal_cache[face]

    def fix_face_orientations(self):
        # Arrange that each face's normal points outwards, by
        # reversing the vertex list of the face if necessary.

        self.normal_cache.clear()

        # Start by finding a point in the middle of the polyhedron, by
        # averaging all the vertices. (That works as long as the
        # polyhedron is convex.)
        centre = vscale(reduce(vadd, self.vertices.values()),
                        1.0 / len(self.vertices))

        for name, vertices in self.faces.items():
            if vdot(vsub(self.vertices[vertices[0]], centre),
                    self.normal(name)) < 0:
                vertices.reverse()

        self.normal_cache.clear()

    def write(self, fh):
        self.fix_face_orientations()

        for name, coords in self.vertices.items():
            print("point", name, *coords, file=fh)

        for name, vertices in self.faces.items():
            for vertex in vertices:
                print("face", name, vertex, file=fh)
            print("normal", name, *self.normal(name), file=fh)

    def get_dicts(self):
        "Dicts of vertex coordinates, face vertex lists, and face normals."
        return (self.vertices.copy(),
                self.faces.copy(),
                {f: self.normal(f) for f in self.faces})

    def has_face(self, face):
        return face in self.faces

    @classmethod
    def read(cls, fh):
        self = cls()
        fr = FileReader(fh)
        for line in iter(fr.readline, None):
            words = line.split(' ')

            if len(words) == 0:
                continue

            if words[0] == "point":
                if len(words) != 5:
                    fr.warning("expected four arguments to 'point'")
                    continue
                try:
                    coords = map(float, words[2:5])
                except ValueError:
                    fr.warning("could not parse point coords as floats")
                    continue
                self.vertex(*coords, name=words[1])

            elif words[0] == "face":
                if len(words) != 3:
                    fr.warning("expected two arguments to 'face'")
                    continue

                face, vertex = words[1:3]
                if vertex not in self.vertices:
                    fr.warning("vertex {} not defined".format(vertex))
                if face not in self.faces:
                    self.face(name=face)
                self.add_to_face(face, vertex)

            elif words[0] == "normal":
                if len(words) != 5:
                    fr.warning("expected four arguments to 'normal'")
                    continue
                try:
                    coords = map(float, words[2:5])
                except ValueError:
                    fr.warning("could not parse normal coords as floats")
                    continue

                # We check that face normals parse, but we ignore
                # them, on the grounds that we can recompute them
                # easily.

            else:
                fr.warning("unrecognised command '{}'".format(words[0]))

        self.fix_face_orientations()
        return self

def crosspoint(xa1,ya1,xa2,ya2,xb1,yb1,xb2,yb2):
    """Give the intersection point of the (possibly extrapolated) line
    segments (xa1,ya1)-(xa2,ya2) and (xb1,yb1)-(xb2,yb2)."""

    # This function is optimised for big-integer or FP arithmetic: it
    # multiplies up to find two big numbers, and then divides them. So
    # if you use it on integers it will give the most accurate answer
    # it possibly can within integers, but might overflow if you don't
    # use longs. I haven't carefully analysed the FP properties, but I
    # can't see it going _too_ far wrong.
    #
    # Of course there's no reason you can't feed it rationals if you
    # happen to have a Rational class. It only does adds, subtracts,
    # multiplies, divides and tests of equality on its arguments, so
    # any data type supporting those would be fine.

    dxa = xa2-xa1
    dya = ya2-ya1
    dxb = xb2-xb1
    dyb = yb2-yb1
    # Special case: if gradients are equal, die.
    if dya * dxb == dxa * dyb:
        return None
    # Second special case: if either gradient is horizontal or
    # vertical.
    if dxa == 0:
        # Because we've already dealt with the parallel case, dxb
        # is now known to be nonzero. So we can simply extrapolate
        # along the b line until it hits the common value xa1==xa2.
        return (xa1, (xa1 - xb1) * dyb / dxb + yb1)
    # Similar cases for dya == 0, dxb == 0 and dyb == 0.
    if dxb == 0:
        return (xb1, (xb1 - xa1) * dya / dxa + ya1)
    if dya == 0:
        return ((ya1 - yb1) * dxb / dyb + xb1, ya1)
    if dyb == 0:
        return ((yb1 - ya1) * dxa / dya + xa1, yb1)

    # General case: all four gradient components are nonzero. In
    # this case, we have
    #
    #     y - ya1   dya           y - yb1   dyb
    #     ------- = ---    and    ------- = ---
    #     x - xa1   dxa           x - xb1   dxb
    #
    # We rewrite these equations as
    #
    #     y = ya1 + dya (x - xa1) / dxa
    #     y = yb1 + dyb (x - xb1) / dxb
    #
    # and equate the RHSes of each
    #
    #     ya1 + dya (x - xa1) / dxa = yb1 + dyb (x - xb1) / dxb
    #  => ya1 dxa dxb + dya dxb (x - xa1) = yb1 dxb dxa + dyb dxa (x - xb1)
    #  => (dya dxb - dyb dxa) x =
    #          dxb dxa (yb1 - ya1) + dya dxb xa1 - dyb dxa xb1
    #
    # So we have a formula for x
    #
    #         dxb dxa (yb1 - ya1) + dya dxb xa1 - dyb dxa xb1
    #     x = -----------------------------------------------
    #                        dya dxb - dyb dxa
    #
    # and by a similar derivation we also obtain a formula for y
    #
    #         dya dyb (xa1 - xb1) + dxb dya yb1 - dxa dyb ya1
    #     y = -----------------------------------------------
    #                        dya dxb - dyb dxa

    det = dya * dxb - dyb * dxa
    xtop = dxb * dxa * (yb1-ya1) + dya * dxb * xa1 - dyb * dxa * xb1
    ytop = dya * dyb * (xa1-xb1) + dxb * dya * yb1 - dxa * dyb * ya1

    return (xtop / det, ytop / det)

def vcrosspoint(a1, a2, b1, b2):
    # Alternative API to crosspoint which takes four vectors in tuple form.
    (xa1, ya1), (xa2, ya2), (xb1, yb1), (xb2, yb2) = (a1, a2, b1, b2)
    return crosspoint(xa1, ya1, xa2, ya2, xb1, yb1, xb2, yb2)

def segments_intersect(xa1,ya1,xa2,ya2,xb1,yb1,xb2,yb2):
    """Decide whether two line segments intersect."""

    # Early termination if the bounding boxes don't overlap.
    if (max(xa1, xa2) < min(xb1, xb2) or max(xb1, xb2) < min(xa1, xa2) or
        max(ya1, ya2) < min(yb1, yb2) or max(yb1, yb2) < min(ya1, ya2)):
        return False

    # Find the crossing point of the two infinite lines (if any).
    cp = crosspoint(xa1,ya1,xa2,ya2,xb1,yb1,xb2,yb2)
    if cp is None:
        return False

    va = vsub((xa2,ya2), (xa1,ya1))
    vb = vsub((xb2,yb2), (xb1,yb1))

    # See if the crossing point is between the ends of each line.
    return (0 <= vdot(vsub(cp,(xa1,ya1)), va) <= vnorm2(va) and
            0 <= vdot(vsub(cp,(xb1,yb1)), vb) <= vnorm2(vb))

def vintersect(a1, a2, b1, b2):
    # Alternative API to segments_intersect which takes four vectors
    # in tuple form.
    (xa1, ya1), (xa2, ya2), (xb1, yb1), (xb2, yb2) = (a1, a2, b1, b2)
    return segments_intersect(xa1, ya1, xa2, ya2, xb1, yb1, xb2, yb2)

def winding_number(point, polygon):
    """Return the winding number of the polygon about the point."""

    # A ray from the point to as far right as we need to go
    xhuge = max(x for x,y in polygon) + 1
    x, y = point

    winding = 0
    for (x0,y0),(x1,y1) in adjacent_tuples(polygon):
        if ((y0 <= y < y1 or y1 <= y < y0) and
            segments_intersect(x, y, xhuge, y, x0, y0, x1, y1)):
            winding += (1 if y0 <= y else -1)

    return winding
