#!/usr/bin/env python3

# Given a set of points on a sphere, construct a polyhedron with
# those points as vertices, by determining the convex hull.

import sys
import string
import random
import argparse
import math
from math import asin, atan2, cos, sin, sqrt

import polylib
from polylib import Matrix, vadd, vsub, vscale

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Construct a polyhedron as the convex hull of a set of '
    'vertex points.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input points file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
args = parser.parse_args()

with polylib.PointReader(args.infile()) as pr:
    points = list(pr)

n = len(points)

# First step: out of the n(n-1)/2 possible edges, determine which
# edges are actually part of the convex hull (and hence appear on
# the outside of the polyhedron).
hulledges = {}
for i in range(n-1):
    pi = points[i]
    for j in range(i+1, n):
        pj = points[j]

        # We begin by rotating our frame of reference so that the
        # vector between our two points is along the z-axis.
        #
        # We'll transform our point set via that matrix, project it
        # into the x-y plane (i.e. the plane perpendicular to the
        # pi<->pj edge), and translate to put the point corresponding
        # to the pi<->pj edge itself at the origin.
        #
        # To compensate for any rounding error in the matrix algebra,
        # we use the midpoint between pi and pj as the basis of that
        # translation.
        matrix = Matrix.rotate_to_z(vsub(pj, pi))
        midpoint = vscale(vadd(pi, pj), 0.5)
        ppoints = [matrix.apply(vsub(points[k], midpoint))[:2]
                   for k in range(n) if k != i and k != j]

        # This has produced a plane projection of the point set, under
        # which the entire edge we're considering becomes a single
        # point at the origin. Now what we do is to see whether that
        # _point_ is on the 2-D convex hull of the projected point
        # set.
        #
        # To do this we will go through all the other points and
        # figure out their bearings from the origin. Then we'll sort
        # those bearings into angle order (which is of course cyclic
        # modulo 2pi). If the origin is part of the convex hull, we
        # expect there to be a gap of size > pi somewhere in that set
        # of angles, indicating that a line can be drawn through the
        # fulcrum at some angle such that all the other points are on
        # the same side of it.

        angles = sorted(atan2(y, x) for x, y in ppoints)
        # Now go through and look for a gap of size pi. There are
        # two possible ways this can happen: either two adjacent
        # elements in the list are separated by more than pi, or
        # the two end elements are separated by _less_ than pi.
        hull = 0
        for k in range(len(angles)-1):
            if angles[k+1] - angles[k] > math.pi:
                hull = 1
                break
        if angles[-1] - angles[0] < math.pi:
            hull = 1

        if hull:
            hulledges[(i,j)] = 1

# Now we know the set of edges involved in the polyhedron, we need
# to combine them into faces. To do this we will have to consider
# each edge, going _both_ ways.
followedges = {}
for i in range(n):
    xi, yi, zi = points[i]
    for j in range(n):
        xj, yj, zj = points[j]
        if i == j: continue
        if not ((i,j) in hulledges or (j,i) in hulledges): continue

        # So we have an edge from point i to point j. We imagine we
        # are walking along that edge from i to j with the
        # intention of circumnavigating the face to our left. So
        # when we reach j, we must turn left on to another edge,
        # and the question is which edge that is.
        #
        # To do this we will begin by rotating so that point j is
        # at (0,0,1). This has been done in several other parts of
        # this code base so I won't comment it in full yet again...
        theta = atan2(yj, xj)
        phi = asin(zj)
        matrix = [
        [ cos(theta)*sin(phi),  sin(theta)*sin(phi),  -cos(phi) ],
        [     -sin(theta)    ,      cos(theta)     ,      0     ],
        [ cos(theta)*cos(phi),  sin(theta)*cos(phi),   sin(phi) ]]

        # Now we are looking directly down on point j. We can see
        # some number of convex-hull edges coming out of it; we
        # determine the angle at which each one emerges, and find
        # the one which is closest to the i-j edge on the left.
        angles = []
        for k in range(n):
            if k == j: continue
            if not ((k,j) in hulledges or (j,k) in hulledges):
                continue
            xk, yk, zk = points[k]
            xk1 = matrix[0][0] * xk + matrix[0][1] * yk + matrix[0][2] * zk
            yk1 = matrix[1][0] * xk + matrix[1][1] * yk + matrix[1][2] * zk
            #zk1 = matrix[2][0] * xk + matrix[2][1] * yk + matrix[2][2] * zk
            angles.append((atan2(xk1, yk1), k))
        # Sort by angle, in reverse order.
        angles.sort(reverse=True)
        # Search for i and take the next thing below it. Wrap
        # round, of course: if angles[0] is i then we want
        # angles[-1]. Conveniently this will be done for us by
        # Python's array semantics :-)
        k = None
        for index in range(len(angles)):
            if angles[index][1] == i:
                k = angles[index-1][1]
                break
        assert k != None
        followedges[(i,j)] = (j,k)

# Now we're about ready to output our polyhedron definition. The
# only thing we're missing is the surface normals, and we'll do
# those as we go along.

poly = polylib.Polyhedron()

# First the vertices.
for i in range(n):
    vlabel = "vertex_" + str(i)
    poly.vertex(*points[i], name=vlabel)

# Now, the faces. We'll simply delete entries from followedges() as
# we go around, so as to avoid doing any face more than once.
while len(followedges) > 0:
    # Pick an arbitrary key in followedges.
    start = this = next(iter(followedges))
    vertices = []
    while 1:
        vertices.append(this[0])
        succ = followedges[this]
        del followedges[this]
        this = succ
        if this == start:
            break
    flabel = "face"
    for i in vertices:
        flabel = flabel + "_" + str(i)
    poly.face(name=flabel)
    for i in vertices:
        vlabel = "vertex_" + str(i)
        poly.add_to_face(flabel, vlabel)

with args.outfile() as fh:
    poly.write(fh)
