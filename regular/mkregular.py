#!/usr/bin/env python3

# Generate polyhedron description files for the regular polyhedra.

import sys
import os
import math
import argparse

# Import polylib from our parent directory
sys.path[1:1] = [os.path.join(sys.path[0], "..")]
import polylib
from polylib import Polyhedron, vunit, vdot, vsub, vnorm, Matrix

def lettername(n):
    chars = 1
    while n >= 26 ** chars:
        n = n - 26 ** chars
        chars = chars + 1
    s = ""
    for i in range(chars):
        k = n % 26
        n = n // 26
        if i == chars-1:
            s = chr(65 + k) + s
        else:
            s = chr(97 + k) + s
    return s

def tetrahedron():
    p = Polyhedron()
    # Easiest way to define a tetrahedron is to take four vertices
    # from the cube.
    a = p.vertex(-1, -1, -1)
    b = p.vertex(-1, +1, +1)
    c = p.vertex(+1, +1, -1)
    d = p.vertex(+1, -1, +1)
    p.face(a, b, c)
    p.face(b, c, d)
    p.face(c, d, a)
    p.face(d, a, b)
    return p

def cube():
    p = Polyhedron()
    a = p.vertex(-1, -1, -1)
    b = p.vertex(-1, -1, +1)
    c = p.vertex(-1, +1, -1)
    d = p.vertex(-1, +1, +1)
    e = p.vertex(+1, -1, -1)
    f = p.vertex(+1, -1, +1)
    g = p.vertex(+1, +1, -1)
    h = p.vertex(+1, +1, +1)
    p.face(a, b, d, c)
    p.face(e, f, h, g)
    p.face(a, c, g, e)
    p.face(b, d, h, f)
    p.face(a, e, f, b)
    p.face(c, g, h, d)
    return p

def octahedron():
    p = Polyhedron()
    a = p.vertex(-1, 0, 0)
    b = p.vertex(+1, 0, 0)
    c = p.vertex(0, -1, 0)
    d = p.vertex(0, +1, 0)
    e = p.vertex(0, 0, -1)
    f = p.vertex(0, 0, +1)
    p.face(a, c, e)
    p.face(a, c, f)
    p.face(a, d, e)
    p.face(a, d, f)
    p.face(b, c, e)
    p.face(b, c, f)
    p.face(b, d, e)
    p.face(b, d, f)
    return p

def dodecahedron():
    p = Polyhedron()
    # Simplest way to define a dodecahedron is to raise a roof on
    # each face of a cube. So we start with the cube vertices.
    a = p.vertex(-1, -1, -1)
    b = p.vertex(-1, -1, +1)
    c = p.vertex(-1, +1, -1)
    d = p.vertex(-1, +1, +1)
    e = p.vertex(+1, -1, -1)
    f = p.vertex(+1, -1, +1)
    g = p.vertex(+1, +1, -1)
    h = p.vertex(+1, +1, +1)
    # This cube has side length 2. So the side length of the
    # dodecahedron will be 2/phi.
    phi = (1.0 + math.sqrt(5))/2.0
    side = 2.0 / phi
    # For a given roof vertex, then...
    #
    #  +--------+
    #  |\      /|
    #  | \____/ |
    #  | /    \ |
    #  |/      \|
    #  +--------+
    #
    # ... the coordinates of the rightmost roof vertex are 0 in one
    # direction, side/2 in another, and z in a third, where z is 1
    # plus a length computed to make the diagonal roof edges equal
    # in length to side. Thus
    #
    #   1^2 + (1-side/2)^2 + (z-1)^2 == side^2
    z = 1 + math.sqrt(side**2 - (1-side/2)**2 - 1)
    y = side/2

    ab = p.vertex(-z, -y, 0)
    cd = p.vertex(-z, +y, 0)
    ef = p.vertex(+z, -y, 0)
    gh = p.vertex(+z, +y, 0)
    ae = p.vertex(0, -z, -y)
    bf = p.vertex(0, -z, +y)
    cg = p.vertex(0, +z, -y)
    dh = p.vertex(0, +z, +y)
    ac = p.vertex(-y, 0, -z)
    eg = p.vertex(+y, 0, -z)
    bd = p.vertex(-y, 0, +z)
    fh = p.vertex(+y, 0, +z)

    p.face(a, ab, b, bf, ae)
    p.face(e, ef, f, bf, ae)
    p.face(c, cd, d, dh, cg)
    p.face(g, gh, h, dh, cg)
    p.face(a, ac, c, cd, ab)
    p.face(b, bd, d, cd, ab)
    p.face(e, eg, g, gh, ef)
    p.face(f, fh, h, gh, ef)
    p.face(a, ae, e, eg, ac)
    p.face(c, cg, g, eg, ac)
    p.face(b, bf, f, fh, bd)
    p.face(d, dh, h, fh, bd)

    return p

def snubcube():
    # https://en.wikipedia.org/wiki/Snub_cube#Cartesian_coordinates

    def tribonacci():
        # The real root of x^3-x^2-x-1
        t = 1/3
        p = 4/9
        z3 = -(3*math.sqrt(33)-19)/27
        z = math.pow(z3, 1/3)
        y = z + p/z
        x = y + t
        return x
    t = tribonacci()
    T = 1/t

    p = Polyhedron()

    XY = p.vertex(-t,-1,-T, name="vXY")
    Xy = p.vertex(-t,+1,+T, name="vXy")
    XZ = p.vertex(-t,+T,-1, name="vXZ")
    Xz = p.vertex(-t,-T,+1, name="vXz")

    xY = p.vertex(+t,-1,+T, name="vxY")
    xy = p.vertex(+t,+1,-T, name="vxy")
    xZ = p.vertex(+t,-T,-1, name="vxZ")
    xz = p.vertex(+t,+T,+1, name="vxz")

    YZ = p.vertex(-T,-t,-1, name="vYZ")
    Yz = p.vertex(+T,-t,+1, name="vYz")
    YX = p.vertex(-1,-t,+T, name="vYX")
    Yx = p.vertex(+1,-t,-T, name="vYx")

    yZ = p.vertex(+T,+t,-1, name="vyZ")
    yz = p.vertex(-T,+t,+1, name="vyz")
    yX = p.vertex(-1,+t,-T, name="vyX")
    yx = p.vertex(+1,+t,+T, name="vyx")

    ZX = p.vertex(-1,-T,-t, name="vZX")
    Zx = p.vertex(+1,+T,-t, name="vZx")
    ZY = p.vertex(+T,-1,-t, name="vZY")
    Zy = p.vertex(-T,+1,-t, name="vZy")

    zX = p.vertex(-1,+T,+t, name="vzX")
    zx = p.vertex(+1,-T,+t, name="vzx")
    zY = p.vertex(-T,-1,+t, name="vzY")
    zy = p.vertex(+T,+1,+t, name="vzy")

    p.face(XY, XZ, Xy, Xz, name="fX")
    p.face(xY, xZ, xy, xz, name="fx")
    p.face(YZ, YX, Yz, Yx, name="fY")
    p.face(yZ, yX, yz, yx, name="fy")
    p.face(ZX, ZY, Zx, Zy, name="fZ")
    p.face(zX, zY, zx, zy, name="fz")
    p.face(XY, YZ, ZX, name="fXYZ")
    p.face(Xz, YX, zY, name="fXYz")
    p.face(XZ, Zy, yX, name="fXyZ")
    p.face(Xy, yz, zX, name="fXyz")
    p.face(Yx, ZY, xZ, name="fxYZ")
    p.face(Yz, xY, zx, name="fxYz")
    p.face(Zx, xy, yZ, name="fxyZ")
    p.face(xz, yx, zy, name="fxyz")
    p.face(XZ, Xy, yX, name="fXy")
    p.face(XY, XZ, ZX, name="fXZ")
    p.face(XY, Xz, YX, name="fXY")
    p.face(Xy, Xz, zX, name="fXz")
    p.face(Yx, xY, xZ, name="fxY")
    p.face(Zx, xZ, xy, name="fxZ")
    p.face(xY, xz, zx, name="fxz")
    p.face(xy, xz, yx, name="fxy")
    p.face(YX, Yz, zY, name="fYz")
    p.face(Yx, Yz, xY, name="fYx")
    p.face(XY, YX, YZ, name="fYX")
    p.face(YZ, Yx, ZY, name="fYZ")
    p.face(xy, yZ, yx, name="fyx")
    p.face(yx, yz, zy, name="fyz")
    p.face(Xy, yX, yz, name="fyX")
    p.face(Zy, yX, yZ, name="fyZ")
    p.face(XZ, ZX, Zy, name="fZX")
    p.face(YZ, ZX, ZY, name="fZY")
    p.face(ZY, Zx, xZ, name="fZx")
    p.face(Zx, Zy, yZ, name="fZy")
    p.face(Xz, zX, zY, name="fzX")
    p.face(yz, zX, zy, name="fzy")
    p.face(xz, zx, zy, name="fzx")
    p.face(Yz, zY, zx, name="fzY")

    p.fix_face_orientations()

    return p

def snubdodecahedron():
    # https://en.wikipedia.org/wiki/Snub_dodecahedron#Cartesian_coordinates

    phi = (1 + math.sqrt(5))/2
    def root():
        # The real root of x^3+2x^2-phi^2
        t = -2/3
        p = 4/9
        z3= -(3 * math.sqrt(49*math.sqrt(5)+93) * math.sqrt(6) -
              27 * math.sqrt(5) - 49) / 108
        z = math.pow(z3, 1/3)
        y = z + p/z
        x = y + t
        return x
    zeta = root()

    p = [
        phi**2 * (1-zeta),
        -phi**3 + phi*zeta + 2*phi*zeta**2,
        zeta
    ]

    M1 = Matrix.fromrows(
        [1/(2*phi), -phi/2, 1/2],
        [phi/2, 1/2, 1/(2*phi)],
        [-1/2, 1/(2*phi), phi/2],
    )
    M2 = Matrix.fromrows(
        [0, 0, 1],
        [1, 0, 0],
        [0, 1, 0],
    )

    points = [p]
    qhead = 0

    def index(q):
        for i, r in enumerate(points):
            if vnorm(vsub(q, r)) < 1e-3:
                return i
        i = len(points)
        points.append(q)
        return i

    T1 = []
    T2 = []

    while qhead < len(points):
        q = points[qhead]
        qhead += 1
        T1.append(index(M1.apply(q)))
        T2.append(index(M2.apply(q)))

    assert len(points) == 60 # that should be all the vertices

    edgelen = 2*zeta*math.sqrt(1-zeta)
    edgeset = {(pi, qi)
               for pi, p in enumerate(points)
               for qi, q in enumerate(points)
               if abs(vnorm(vsub(p, q)) - edgelen) < 1e-3}

    assert len(edgeset) == 300 # 150 edges, each counted in both directions

    adj = {pi: set() for pi in range(len(points))}
    for pi in range(len(points)):
        for qi in range(len(points)):
            if (pi, qi) in edgeset:
                adj[pi].add(qi)

    assert all(len(v) == 5 for v in adj.values()) # each vertex has degree 4

    # For each vertex, generate its four triangular faces and also
    # find the subgraph of edges that border pentagonal faces.
    triangles = set()
    padj = {pi: set() for pi in range(len(points))}
    for v in range(len(points)):
        ntri = {u: 0 for u in adj[v]}
        for u in adj[v]:
            for w in adj[v]:
                if (u, w) in edgeset:
                    triangles.add(tuple(sorted([u, v, w])))
                    ntri[u] += 1
                    ntri[w] += 1
        for u, count in ntri.items():
            if count == 2: # expect to have counted the triangle in both senses
                padj[u].add(v)
                padj[v].add(u)

    assert len(triangles) == 80
    assert all(len(v) == 2 for v in padj.values()) # should be 2-regular

    # Now construct the pentagonal faces by walking round each cycle
    # of the padj subgraph
    pentagons = set()
    for v in range(len(points)):
        prev = None
        vs = []
        for _ in range(5):
            vs.append(v)
            prev, v = v, next(w for w in padj[v] if w != prev)
        vsn = min([[vs[(start+i*step) % 5] for i in range(5)]
                   for start in range(5) for step in [1, -1]])
        pentagons.add(tuple(vsn))

    assert len(pentagons) == 12

    p = Polyhedron()
    pvs = [p.vertex(*v) for v in points]
    for f in sorted(triangles) + sorted(pentagons):
        p.face(*[pvs[i] for i in f])
    p.fix_face_orientations()

    return p

def schulz_dodecahedron():
    # A (probably) minimal solid that is graph-theoretically
    # equivalent to a regular dodecahedron, but has all the vertices
    # at integer lattice points. Source (and name):
    # https://mathoverflow.net/q/234212

    p = Polyhedron()

    C = p.vertex(+2, +2, +2)
    L = p.vertex(+1, +2, +2)
    M = p.vertex(+2, +1, +2)
    N = p.vertex(+2, +2, +1)
    U = p.vertex(+2,  0, -1)
    V = p.vertex(+2, -1,  0)
    W = p.vertex(-1, +2,  0)
    X = p.vertex( 0, +2, -1)
    Y = p.vertex( 0, -1, +2)
    Z = p.vertex(-1,  0, +2)

    c = p.vertex(-2, -2, -2)
    l = p.vertex(-1, -2, -2)
    m = p.vertex(-2, -1, -2)
    n = p.vertex(-2, -2, -1)
    u = p.vertex(-2,  0, +1)
    v = p.vertex(-2, +1,  0)
    w = p.vertex(+1, -2,  0)
    x = p.vertex( 0, -2, +1)
    y = p.vertex( 0, +1, -2)
    z = p.vertex(+1,  0, -2)

    p.face(C, M, Y, Z, L)
    p.face(C, N, U, V, M)
    p.face(C, L, W, X, N)
    p.face(L, Z, u, v, W)
    p.face(M, V, w, x, Y)
    p.face(N, X, y, z, U)

    p.face(c, l, z, y, m)
    p.face(c, m, v, u, n)
    p.face(c, n, x, w, l)
    p.face(l, w, V, U, z)
    p.face(m, y, X, W, v)
    p.face(n, u, Z, Y, x)

    return p

def icosahedron():
    p = Polyhedron()
    # The classical easy way to define an icosahedron in Cartesian
    # coordinates is to imagine three interlocking cards along the
    # three axes, each in golden ratio.
    phi = (1.0 + math.sqrt(5))/2.0

    a = p.vertex(0, -1, -phi)
    b = p.vertex(0, -1, +phi)
    c = p.vertex(0, +1, -phi)
    d = p.vertex(0, +1, +phi)

    e = p.vertex(-1, -phi, 0)
    f = p.vertex(-1, +phi, 0)
    g = p.vertex(+1, -phi, 0)
    h = p.vertex(+1, +phi, 0)

    i = p.vertex(-phi, 0, -1)
    j = p.vertex(+phi, 0, -1)
    k = p.vertex(-phi, 0, +1)
    l = p.vertex(+phi, 0, +1)

    # Now. We have a bunch of faces which use the short edge of one
    # of the cards.
    p.face(a, c, i)
    p.face(a, c, j)
    p.face(b, d, k)
    p.face(b, d, l)
    p.face(e, g, a)
    p.face(e, g, b)
    p.face(f, h, c)
    p.face(f, h, d)
    p.face(i, k, e)
    p.face(i, k, f)
    p.face(j, l, g)
    p.face(j, l, h)

    # And then we have eight more faces which each use one vertex
    # from each card.
    p.face(a, e, i)
    p.face(a, g, j)
    p.face(b, e, k)
    p.face(b, g, l)
    p.face(c, f, i)
    p.face(c, h, j)
    p.face(d, f, k)
    p.face(d, h, l)

    return p

def associahedron():
    # The 5-associahedron (that is, the 3-dimensional one), realised
    # as the secondary polytope of a regular hexagon.
    #
    # In other words: this is a geometric construction which
    # considers, for each possible triangulation of the hexagon
    # (corresponding to a vertex of the associahedron), the total area
    # of all triangles touching each vertex of the hexagon. This
    # assigns a point in R^6 to each associahedron vertex. It will
    # then turn out that all of those points are in an affine
    # 3-subspace, so we can translate and rotate them into R^3.
    #
    # To begin with, if we consider our hexagon to be made up of six
    # equilateral triangles of unit area, then there are three
    # possible triangles that can appear in triangulations of it, with
    # areas 1, 2, 3:
    #     _______          _____   
    #    /|     /\        /|`.  \  
    #   / | 2  /  \      / |  `. \ 
    #  / 1|   /    \    /  |  3 `.\
    #  \  |  /     /    \  |    ,'/
    #   \ | /     /      \ |  ,' / 
    #    \|/_____/        \|,'__/  
    #
    # and, up to symmetry, basically three interesting kinds of
    # triangulation, with vertex areas marked:
    #
    #    3_______5        3_______4         5____1
    #    /|     /\        /|     /\        /|`. 1\
    #   / | 2  /| \      / | 2  /  \      / |  `. \
    # 1/ 1|   / |1 \1  1/ 1|   / 2 _\3  1/ 1|  3 `.\5
    #  \  |  /  |  /    \  |  /  .' /    \  |    ,'/
    #   \ | / 2 | /      \ | / .'  /      \ |  ,'1/
    #    \|/____|/        \|/_'__1/        \|,'__/
    #    5       3         6      1         5    1
    #
    # So our points in R^6 consist of all rotations and reversals of
    # the three vectors [1,3,5,1,3,5], [1,3,4,3,1,6] and [1,5,1,5,1,5].
    #
    # It's a tedious but not too hard linear algebra exercise to find
    # a set of three mutually orthogonal unit vectors in R^6 spanning
    # the affine space in which all of those lie. The ones I picked
    # were these:
    e1 = vunit([ -1,  1, -1,  1, -1,  1 ])
    e2 = vunit([ -1, -1,  2, -1, -1,  2 ])
    e3 = vunit([ -1,  1,  0, -1,  1,  0 ])
    def transform(v6):
        return (vdot(e1,v6), vdot(e2,v6), vdot(e3,v6))

    p = Polyhedron()
    a = p.vertex(*transform([5, 1, 5, 1, 5, 1])) # [-sqrt(24), 0, 0]
    b = p.vertex(*transform([3, 1, 6, 1, 3, 4])) # [-sqrt(6), sqrt(12), 0]
    c = p.vertex(*transform([3, 4, 3, 1, 6, 1])) # [-sqrt(6), -sqrt(3), 3]
    d = p.vertex(*transform([6, 1, 3, 4, 3, 1])) # [-sqrt(6), -sqrt(3), -3]
    e = p.vertex(*transform([3, 1, 5, 3, 1, 5])) # [0, sqrt(12), -2]
    f = p.vertex(*transform([1, 3, 5, 1, 3, 5])) # [0, sqrt(12), 2]
    g = p.vertex(*transform([1, 5, 3, 1, 5, 3])) # [0, 0, 4]
    h = p.vertex(*transform([3, 5, 1, 3, 5, 1])) # [0, -sqrt(12), 2]
    i = p.vertex(*transform([5, 3, 1, 5, 3, 1])) # [0, -sqrt(12), -2]
    j = p.vertex(*transform([5, 1, 3, 5, 1, 3])) # [0, 0, -4]
    k = p.vertex(*transform([1, 3, 4, 3, 1, 6])) # [sqrt(6), sqrt(12), 0]
    l = p.vertex(*transform([1, 6, 1, 3, 4, 3])) # [sqrt(6), -sqrt(3), 3]
    m = p.vertex(*transform([4, 3, 1, 6, 1, 3])) # [sqrt(6), -sqrt(3), -3]
    n = p.vertex(*transform([1, 5, 1, 5, 1, 5])) # [sqrt(24), 0, 0]
    p.face(a,b,f,g,c)
    p.face(a,c,h,i,d)
    p.face(a,d,j,e,b)
    p.face(b,f,k,e)
    p.face(c,g,l,h)
    p.face(d,i,m,j)
    p.face(n,k,f,g,l)
    p.face(n,l,h,i,m)
    p.face(n,m,j,e,k)
    return p

def snubdisphenoid():
    # Via https://en.wikipedia.org/wiki/Snub_disphenoid#Cartesian_coordinates

    # Construct the real root of 2x^3 + 11x^2 + 4x - 1
    def root():
        t = -11/6
        p = 97/36
        z3r = -881/216
        z3i = -math.sqrt(237)/9
        zm = math.pow(math.hypot(z3i, z3r), 1/3)
        za = math.atan2(z3i, z3r) / 3
        y = (zm + p/zm) * math.cos(za)
        x = y + t
        return x
    q = root()

    r = math.sqrt(q)
    s = math.sqrt((1 - q) / (2*q))
    t = 2*r*s

    p = Polyhedron()

    # This snub disphenoid is best imagined as being oriented with the
    # y-axis vertical.
    #
    # The solid consists of a top edge and a bottom edge, each in the
    # horizontal (xz) plane, at right angles to each other; and in
    # between, an equatorial diamond of four additional vertices, with
    # two of them in the vertical plane of the top edge, and the other
    # two in the vertical plane of the bottom edge. But the equator
    # vertices aren't all in the same _horizontal_ plane: the vertices
    # in the plane of the top edge are lower down, and those in the
    # plane of the bottom edge are higher up, to put them at the right
    # distances from everything.
    #
    # Then the edges consist of the top and bottom edges; the
    # equatorial edges; and each top/bottom vertex is connected to
    # _three_ of the four equatorial edges, excluding the one in its
    # own plane on the far side.
    #
    # With the coordinates constructed in this manner, all edges of
    # the solid have length 2.

    # a,b,c,d are the equatorial vertices.
    a = p.vertex(t, r, 0)
    b = p.vertex(0, -r, t)
    c = p.vertex(-t, r, 0)
    d = p.vertex(0, -r, -t)

    # e,f form the bottom edge. It's in the x-y vertical plane, so a,c
    # are 'its' equator vertices. Hence, both of e,f will join to b,d,
    # but each one will only join to the nearer one of a,c.
    e = p.vertex(1, -s, 0)
    f = p.vertex(-1, -s, 0)

    # Conversely, g,h form the top edge, in the y-z vertical plane.
    # These will each link to both of a,c, but each to only one of b,d.
    g = p.vertex(0, s, 1)
    h = p.vertex(0, s, -1)

    p.face(a, b, e)
    p.face(a, g, b)
    p.face(a, e, d)
    p.face(a, d, h)
    p.face(a, h, g)
    p.face(c, f, b)
    p.face(c, b, g)
    p.face(c, d, f)
    p.face(c, h, d)
    p.face(c, g, h)
    p.face(b, f, e)
    p.face(d, e, f)

    return p

def edges(p):
    # Return a list of all the edges of a polyhedron, in the form
    # of a list of 2-tuples containing vertex names.
    elist = []
    for f in p.faces:
        for v1, v2 in polylib.adjacent_tuples(p.faces[f]):
            if v1 < v2: # only add each edge once
                elist.append((v1,v2))
    return elist

def edgemap(p):
    # Return a mapping from ordered vertex pairs to the face
    # containing that edge.
    emap = {}
    for f in p.faces:
        for v1, v2 in polylib.adjacent_tuples(p.faces[f]):
            assert (v1,v2) not in emap
            emap[(v1,v2)] = f
    return emap

class dual:
    # This wrapper class takes as input a function returning a
    # polyhedron object, and implements a callable object which
    # returns the dual of that polyhedron.
    def __init__(self, p):
        self.p = p
    def __call__(self):
        p = self.p()
        p.fix_face_orientations()
        pout = Polyhedron()

        # The vertices of the new polyhedron are constructed from
        # the faces of the original.
        self.f2v = {}
        for key in p.faces:
            # We already have a normal vector for the face. So take
            # the dot product of that normal vector with each
            # vertex on the face, take the average (just in case),
            # invert the length, and there's our vertex.
            nx, ny, nz = p.normal(key)
            nl = math.sqrt(nx*nx + ny*ny + nz*nz)
            nx, ny, nz = nx/nl, ny/nl, nz/nl
            dps = dpn = 0
            for v in p.faces[key]:
                x, y, z = p.vertices[v]
                dps = dps + x*nx + y*ny + z*nz
                dpn = dpn + 1
            dist = dpn / dps   # reciprocal of average of dot products
            nx, ny, nz = nx * dist, ny * dist, nz * dist
            self.f2v[key] = pout.vertex(nx, ny, nz)

        edges = edgemap(p)

        # Now we can output the faces, one (of course) for each
        # vertex of the old polyhedron.
        self.v2f = {}
        for key in p.vertices:
            # Start by finding one face of the old polyhedron
            # which contains this vertex. This gives us one
            # vertex of the new one which is on this face.
            fstart = None
            for f in p.faces:
                if key in p.faces[f]:
                    fstart = f
                    break
            assert fstart != None

            f = fstart
            vl = []
            while 1:
                # Now find the edge of that face which comes
                # _in_ to that vertex, and then look its
                # reverse up in the edge database. This gives
                # us the next face going round.
                vl.append(self.f2v[f])
                fl = p.faces[f]
                i = fl.index(key)
                v1, v2 = fl[i-1], fl[i]
                f = edges[(v2,v1)]
                if f == fstart:
                    break
            self.v2f[key] = pout.face(*vl)

        return pout

class edgedual:
    # This wrapper class takes as input two functions returning
    # dual polyhedron objects, and implements a callable object
    # which returns the edge dual of those polyhedra.
    #
    # FIXME: this is currently unused (since dual() does a better
    # job of generating the rhombic dodecahedron and rhombic
    # triacontahedron), and also it would benefit from only needing
    # one polyhedron as input (since it could construct the dual
    # adequately as it went).
    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2
    def __call__(self):
        p1 = self.p1()
        p2 = self.p2()
        pout = Polyhedron()
        # Find the edges of each.
        e1 = edges(p1)
        e2 = edges(p2)
        # For each polyhedron, find the radius vectors from the
        # origin to the middle of each edge. As a by-produt of this
        # phase, we also find the scale factor which normalises
        # each polyhedron to the size where those radii have length
        # 1.
        r1 = {}
        r2 = {}
        for p, e, r in (p1,e1,r1),(p2,e2,r2):
            sl = sn = 0
            for v1, v2 in e:
                x1, y1, z1 = p.vertices[v1]
                x2, y2, z2 = p.vertices[v2]
                x, y, z = (x1+x2)/2, (y1+y2)/2, (z1+z2)/2
                l = math.sqrt(x*x+y*y+z*z)
                r[(v1,v2)] = x, y, z
                sl = sl + l
                sn = sn + 1
            r[None] = sn / sl
        # Now match up the edges between the two polyhedra. As a
        # test of duality, we deliberately check that the mapping
        # we end up with is a bijection.
        emap = {}
        scale1 = r1[None]
        scale2 = r2[None]
        for e in e2:
            n = r2[e]
            n = (n[0]*scale2, n[1]*scale2, n[2]*scale2)
            best = None
            bestdist = None
            for eprime in e1:
                nprime = r1[eprime]
                nprime = (nprime[0]*scale2, nprime[1]*scale2, nprime[2]*scale2)
                dist = ((nprime[0]-n[0])**2 +
                        (nprime[1]-n[1])**2 + (nprime[2]-n[2])**2)
                if bestdist == None or dist < bestdist:
                    bestdist = dist
                    best = eprime
            # Ensure the function from e2 to e1 is injective, by
            # making sure we never overwrite an entry in emap.
            assert best not in emap
            emap[best] = e
        # Ensure the function from e2 to e1 is surjective, by
        # making sure everything in e1 is covered.
        assert len(emap) == len(e1)

        # Output the edge dual's vertices.
        vmap1 = {}
        vmap2 = {}
        for p, r, vmap in (p1,r1,vmap1),(p2,r2,vmap2):
            scale = r[None]
            for v in p.vertices:
                x, y, z = p.vertices[v]
                vmap[v] = pout.vertex(x*scale, y*scale, z*scale)

        # And output the faces.
        for e1, e2 in emap.items():
            pout.face(vmap1[e1[0]], vmap2[e2[0]], vmap1[e1[1]], vmap2[e2[1]])

        return pout

def edgeratio(x, y):
    # Determine the ratio between the edge lengths of an x-sided
    # and y-sided regular polygon with the same inscribed circle.
    return math.tan(math.pi/y) / math.tan(math.pi/x)

class truncate:
    # This wrapper class takes a function returning a polyhedron
    # object, and implements a callable object which returns a
    # truncated form of that polyhedron. Parametrised by a single
    # real, which indicates the proportion of the original edges to
    # leave. (0 means truncate at the midpoint; 1 means the null
    # truncation right at the end.)
    def __init__(self, p, r):
        self.p = p
        self.r = r
    def __call__(self):
        p = self.p()
        p.fix_face_orientations()
        pout = Polyhedron()
        e = edges(p)
        newv = {}
        r1 = (1.0 + self.r) / 2
        r2 = (1.0 - self.r) / 2
        for v1, v2 in e:
            # Find the locations of each vertex.
            x1, y1, z1 = p.vertices[v1]
            x2, y2, z2 = p.vertices[v2]
            # Find the truncation points.
            xa, ya, za = r1*x1+r2*x2, r1*y1+r2*y2, r1*z1+r2*z2
            va = pout.vertex(xa, ya, za)
            if self.r != 0:
                xb, yb, zb = r2*x1+r1*x2, r2*y1+r1*y2, r2*z1+r1*z2
                vb = pout.vertex(xb, yb, zb)
                newv[(v1,v2)] = [va,vb]
                newv[(v2,v1)] = [vb,va]
            else:
                newv[(v1,v2)] = newv[(v2,v1)] = [va]
        # Now we have our vertex set. Construct the truncated faces.
        for f in p.faces:
            newvl = []
            for v1, v2 in polylib.adjacent_tuples(p.faces[f]):
                newvl.extend(newv[(v1,v2)])
            pout.face(*newvl)
        # And also we need to construct the _truncation_ faces.
        em = edgemap(p)
        for v in p.vertices:
            # First find a vertex connected to v.
            vc = None
            for v1, v2 in e:
                if v1 == v:
                    vc = v2
                    break
                elif v2 == v:
                    vc = v1
                    break
            assert vc != None
            # Now repeatedly find the (v,vc) edge, and use the
            # edgemap to find another vertex connected to v.
            newvl = []
            vs = vc
            while 1:
                newvl.append(newv[(v,vc)][0])
                f = em[(v,vc)]
                fl = p.faces[f]
                i = fl.index(v)
                vc = fl[i-1]  # -1 wraps
                if vc == vs:
                    break
            pout.face(*newvl)

        return pout

class rhombi:
    # This wrapper class takes a function returning a polyhedron
    # object, and constructs one of the `rhombi' forms related to
    # it and its dual.
    def __init__(self, p, forder, vorder, great):
        self.p = p
        self.forder = forder
        self.vorder = vorder
        self.great = great
    def __call__(self):
        p = self.p()
        p.fix_face_orientations()
        pout = Polyhedron()

        if self.great:
            pratio = edgeratio(self.forder, self.forder*2)
            dratio = edgeratio(self.vorder, self.vorder*2)

        # Start by finding the polyhedron's dual, and retrieving
        # the correspondence mappings from the dual object which
        # indicate which faces go with which vertices and vice
        # versa.
        dobj = dual(self.p)
        d = dobj()
        f2v = dobj.f2v
        v2f = dobj.v2f

        e = edges(p)
        em = edgemap(p)
        de = edges(d)

        # Find the scale factor which makes the two polyhedra have
        # compatible face lengths. (For a small rhombi form, this
        # is just a question of matching the edge lengths. For a
        # great one, we must match the _post-truncation_ edge
        # lengths.)
        e1 = e[0]
        x1, y1, z1 = p.vertices[e1[0]]
        x2, y2, z2 = p.vertices[e1[1]]
        elen = math.sqrt((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)
        e1 = de[0]
        x1, y1, z1 = d.vertices[e1[0]]
        x2, y2, z2 = d.vertices[e1[1]]
        delen = math.sqrt((x2-x1)**2 + (y2-y1)**2 + (z2-z1)**2)
        if self.great:
            elen = elen * pratio
            delen = delen * dratio
        scale = elen / delen
        # Actually scale the dual polyhedron object.
        for dv in d.vertices.keys():
            x, y, z = d.vertices[dv]
            d.vertices[dv] = x*scale, y*scale, z*scale

        # Find the distance by which each face must be pushed out
        # before they meet in the right way. For a small rhombi
        # form, this means making vertices of adjacent faces meet;
        # for a great one, post-truncation vertices of adjacent
        # faces (or equivalently the midpoints of truncation edges)
        # must meet.
        #
        # To do this, we first find a face of p and a face of d
        # which are `adjacent' in the sense that each face
        # corresponds to a vertex of the other face. Then we
        # determine a pair of points which we intend to bring into
        # congruence (either vertices of the original faces, or
        # midpoints of truncation edges). Then we mentally draw
        # this diagram
        #
        #       O
        #       |\
        #       | \    ,D
        #       |  \ ,'
        #       |  ,Q
        #   A---P---B
        #       C   |
        #        \  |
        #         \ |
        #          \|
        #           X
        #
        # Here O is the origin; the lines APB and CQD are the
        # projections of the two faces; and B and C are the points
        # we are trying to bring together at X.
        #
        # A quick bit of trig suggests that the distance XO
        # measured parallel to OP is equal to
        #   (PB / tan POQ) + (QC / sin POQ)
        # and hence by symmetry XO measured perpendicular to CD is
        # equal to
        #   (QC / tan POQ) + (PB / sin POQ).
        e1 = e[0]
        v = e1[0]
        f = em[e1]
        dv = f2v[f]
        df = v2f[v]
        # Find the angle theta (POQ above) between the two faces:
        # arccos of the dot product of the normal vectors.
        nx1, ny1, nz1 = p.normal(f)
        nx2, ny2, nz2 = d.normal(df)
        theta = math.acos(nx1*nx2 + ny1*ny2 + nz1*nz2)
        # Find the two distances PB and QC.
        if self.great:
            # The distance from the centre of a face to the
            # midpoint of a truncation edge will be equal to the
            # distance from the centre of the face to the midpoint
            # of the original edge, so we'll just measure that for
            # the moment.
            x1, y1, z1 = p.vertices[e1[0]]
            x2, y2, z2 = p.vertices[e1[1]]
            px, py, pz = (x1+x2)/2, (y1+y2)/2, (z1+z2)/2
            # Now the same in the dual.
            x1, y1, z1 = d.vertices[f2v[em[e1]]]
            x2, y2, z2 = d.vertices[f2v[em[(e1[1],e1[0])]]]
            dx, dy, dz = (x1+x2)/2, (y1+y2)/2, (z1+z2)/2
        else:
            # We want the original vertices of the original faces
            # to come into congruence, so we measure the centre-to-
            # vertex distance for a face of each polyhedron.
            px, py, pz = p.vertices[v]
            dx, dy, dz = d.vertices[dv]
        pdp = px * nx1 + py * ny1 + pz * nz1
        cx, cy, cz = nx1 * pdp, ny1 * pdp, nz1 * pdp
        pdist = math.sqrt((px-cx)**2 + (py-cy)**2 + (pz-cz)**2)
        # Now the same in the dual.
        ddp = dx * nx2 + dy * ny2 + dz * nz2
        cx, cy, cz = nx2 * ddp, ny2 * ddp, nz2 * ddp
        ddist = math.sqrt((dx-cx)**2 + (dy-cy)**2 + (dz-cz)**2)
        # Now we can compute the extra distance we must push the p
        # faces and the d faces.
        ppush = (pdist / math.tan(theta)) + (ddist / math.sin(theta)) - pdp
        dpush = (ddist / math.tan(theta)) + (pdist / math.sin(theta)) - ddp

        # Construct the actual vertices of the solid. Every vertex
        # occurs precisely twice, once due to a transformed face of
        # the original solid and once due to a transformed face of
        # the dual. Probably easiest to go with only one of these.
        #
        # For a great rhombi form, this is where we truncate.
        pvmap = {} # maps (face,vertex,othervertex) of p to new vertex
        for f in p.faces:
            nx, ny, nz = p.normal(f)
            px, py, pz = nx * ppush, ny * ppush, nz * ppush
            for v0, v1, v2 in polylib.adjacent_tuples(p.faces[f], 3):
                x1, y1, z1 = p.vertices[v1]
                x2, y2, z2 = p.vertices[v2]
                if self.great:
                    # Truncate.
                    r1 = (1.0 + pratio) / 2
                    r2 = (1.0 - pratio) / 2
                    xa, ya, za = r1*x1+r2*x2, r1*y1+r2*y2, r1*z1+r2*z2
                    xb, yb, zb = r2*x1+r1*x2, r2*y1+r1*y2, r2*z1+r1*z2
                    pvmap[(f, v1, v2)] = pout.vertex(xa+px, ya+py, za+pz)
                    pvmap[(f, v2, v1)] = pout.vertex(xb+px, yb+py, zb+pz)
                else:
                    nv = pout.vertex(x1+px, y1+py, z1+pz)
                    pvmap[(f, v1, v2)] = pvmap[(f, v1, v0)] = nv
        # Build other indexes of the same vertex set.
        pfmap = {} # maps (face,vertex,otherface) of p to new vertex
        for f, v1, v2 in pvmap.keys():
            if em[(v1,v2)] == f:
                f2 = em[(v2,v1)]
            else:
                assert em[(v2,v1)] == f
                f2 = em[(v1,v2)]
            pfmap[f, v1, f2] = pvmap[(f, v1, v2)]
        dvmap = {} # maps (face,vertex,othervertex) of d to new vertex
        for f1, v, f2 in pfmap.keys():
            dvmap[v2f[v], f2v[f1], f2v[f2]] = pfmap[(f1, v, f2)]

        # Enumerate the faces of the original solid.
        for f in p.faces:
            vl = p.faces[f]
            newvl = []
            for i in range(len(vl)):
                v0, v1, v2 = vl[i-2], vl[i-1], vl[i] # -2 and -1 wrap
                va = pvmap[(f, v1, v0)]
                vb = pvmap[(f, v1, v2)]
                newvl.append(va)
                if vb != va:
                    newvl.append(vb)
            pout.face(*newvl)

        # Enumerate the faces of the dual.
        for f in d.faces:
            vl = d.faces[f]
            newvl = []
            for i in range(len(vl)):
                v0, v1, v2 = vl[i-2], vl[i-1], vl[i] # -2 and -1 wrap
                va = dvmap[(f, v1, v0)]
                vb = dvmap[(f, v1, v2)]
                newvl.append(va)
                if vb != va:
                    newvl.append(vb)
            pout.face(*newvl)

        # Finally enumerate the square faces due to the edges of
        # the original solid.
        for v1, v2 in e:
            f1 = em[(v1,v2)]
            f2 = em[(v2,v1)]
            newvl = []
            newvl.append(pfmap[(f1, v1, f2)])
            newvl.append(pfmap[(f2, v1, f1)])
            newvl.append(pfmap[(f2, v2, f1)])
            newvl.append(pfmap[(f1, v2, f2)])
            pout.face(*newvl)

        return pout

class output:
    # This wrapper class takes a function returning a polyhedron
    # object, and implements a callable object which outputs that
    # polyhedron to stdout.
    def __init__(self, fn):
        self.fn = fn
    def __call__(self, file_opener):
        p = self.fn()
        with file_opener() as fh:
            p.write(fh)

def genall(ignored_opener):
    for name, fn in polyhedra.items():
        if name == "all":
            continue
        fn(lambda: open(name, "w"))

polyhedra = {
"tetrahedron": output(tetrahedron),
"cube": output(cube),
"octahedron": output(octahedron),
"dodecahedron": output(dodecahedron),
"schulzdodecahedron": output(schulz_dodecahedron),
"icosahedron": output(icosahedron),
"cuboctahedron": output(truncate(cube, 0)),
"icosidodecahedron": output(truncate(dodecahedron, 0)),
"truncatedtetrahedron": output(truncate(tetrahedron, edgeratio(3,6))),
"truncatedcube": output(truncate(cube, edgeratio(4,8))),
"truncatedoctahedron": output(truncate(octahedron, edgeratio(3,6))),
"truncateddodecahedron": output(truncate(dodecahedron, edgeratio(5,10))),
"truncatedicosahedron": output(truncate(icosahedron, edgeratio(3,6))),
"smallrhombicuboctahedron": output(rhombi(cube, 4, 3, 0)),
"greatrhombicuboctahedron": output(rhombi(cube, 4, 3, 1)),
"smallrhombicosidodecahedron": output(rhombi(dodecahedron, 5, 3, 0)),
"greatrhombicosidodecahedron": output(rhombi(dodecahedron, 5, 3, 1)),
"rhombicdodecahedron": output(dual(truncate(cube, 0))),
"rhombictriacontahedron": output(dual(truncate(dodecahedron, 0))),
"snubcube": output(snubcube),
"snubdodecahedron": output(snubdodecahedron),
"associahedron": output(associahedron),
"snubdisphenoid": output(snubdisphenoid),
"all": genall,
}

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
default_output_opener = opener("w")("-")
parser = argparse.ArgumentParser(
    description='Generate descriptions of regular polyhedra.')
parser.add_argument("polyhedron", choices=sorted(polyhedra),
                    help="Polyhedron to generate, or 'all'.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=default_output_opener,
                    help="File to write output to (if not 'all').")
args = parser.parse_args()

if args.polyhedron == "all" and args.outfile is not default_output_opener:
    sys.exit("Can't specify an output file in 'all' mode")

polyhedra[args.polyhedron](args.outfile)
