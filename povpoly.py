#!/usr/bin/env python3

# Read in a polyhedron description, and output a POV-Ray fragment
# which describes it for raytracing purposes.

import sys
import os
import string
import random
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib
from polylib import Matrix, vsub

# FIXME: IWBNI we could introduce some options here for
# non-picture-based polyhedron colouring. For a start, simply
# specifying a different solid colour would be good. Also, how
# about something more fun, such as colouring different
# _classes_ of face? By number of vertices, for instance. Or,
# at the very least, permit individual faces to be coloured by
# specifying them by name.

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
default_output_opener = opener("w")("-")
parser = argparse.ArgumentParser(
    description='Convert a polyhedron into a POV-Ray source file.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=default_output_opener,
                    help="File to write output to.")

# Semi-undocumented option which allows you to specify a file
# containing a PostScript fragment. That file is included at the top
# of the output, and is expected to define a function called
# `picture'.
#
# `picture' will then be called once for each face of the polyhedron,
# and passed two arguments. The first argument will be an orthogonal
# matrix (a length-9 array listing the elements of the matrix ordered
# primarily by columns) and the second a height. When called, the
# origin and clipping path will be set appropriately for `picture' to
# transform some sort of spherical graphical description using the
# given matrix, project it on to the plane (z=height), and draw it.
# This allows a net to be drawn which folds up to produce a polyhedron
# covered in a projection of the original spherical image (e.g. an
# icosahedral globe).
parser.add_argument("-p", "--picture", type=opener("r"),
                    help="PostScript picture description [see source code "
                    "for more details].")
args = parser.parse_args()

picres = 1024

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, normals = poly.get_dicts()

def povprint(*a):
    print(*a, file=outfile)

def psprint(*a):
    print(*a, file=psfile)

outfile = args.outfile()

# Bodge: select image-file base name differently depending on whether
# we just opened standard output or a named file.
if args.outfile is default_output_opener:
    imgbasename = "povpoly." + str(os.getpid())
else:
    imgbasename = outfile.name

# Draw each face. To do this we first transform the solid so that
# that face is in the x-y plane and contained within [0,1]; this is
# not strictly necessary when outputting a plain polyhedron, but
# becomes important when we map an image on to each face.
povprint("union {")
for f in faces.keys():
    n = len(faces[f])
    print(n, faces[f])

    # Compute the matrix we're going to use to transform the [0,1]
    # square into our final polygon. We start by mapping the z unit
    # vector to our surface normal.
    matrixinv = Matrix.rotate_to_z(normals[f])
    matrix = matrixinv.transpose()

    # Applying that matrix to all points _should_ map all the
    # z-coordinates to the same value (although we don't know, or
    # really care, what that value is). Just to be sure, though, we'll
    # average them out now and set them all to _exactly_ the same
    # value.
    #
    # In this loop we also determine the minimum and maximum x- and
    # y-coordinates, so we can stretch our face into [0,1]^2.
    plist = []
    ztotal = 0
    xmin = xmax = None
    ymin = ymax = None
    #zmin = zmax = None
    for v in faces[f]:
        pt = matrixinv.apply(vertices[v])
        plist.append(pt)
        ztotal += pt[2]
        if xmin == None or xmin > pt[0]: xmin = pt[0]
        if xmax == None or xmax < pt[0]: xmax = pt[0]
        if ymin == None or ymin > pt[1]: ymin = pt[1]
        if ymax == None or ymax < pt[1]: ymax = pt[1]
        #if zmin == None or zmin > pt[2]: zmin = pt[2]
        #if zmax == None or zmax < pt[2]: zmax = pt[2]
    zaverage = ztotal / float(n)
    #print "zspread", zmin-zaverage, zmax-zaverage

    scale = max(xmax,-xmin,ymax,-ymin) * 2.0

    # If we're drawing a picture on the polyhedron, we now have all
    # the information we need to make a sub-call to PostScript to
    # render the image for this face.
    if args.picture is not None:
        facebasename = imgbasename + "." + f
        facepsname = facebasename + ".ps"
        facepngname = facebasename + ".png"
        psfile = open(facepsname, "w")
        pic = args.picture()
        while 1:
            s = pic.readline()
            if s == "": break
            psfile.write(s)
        pic.close()
        psprint("36 dup translate")
        psprint("{:.17g} dup scale".format(72.0/scale))
        psprint("[")
        for col in matrixinv.columns():
            psprint(*col)
        psprint("] {:.17g} picture".format(zaverage))
        psprint("showpage")
        psfile.close()
        cmd = ("gs -sDEVICE=png16m -sOutputFile={} -r{:d} -g{:d}x{:d} "
               "-dBATCH -dNOPAUSE -q {}".format(
                   facepngname, picres, picres, picres, facepsname))
        sys.stderr.write(cmd + "\n")
        os.system(cmd)
        pigment = "image_map {{ png \"{}\" }}".format(facepngname)
    else:
        pigment = "solid White"

    # Now stretch the x and y coordinates so that the polygon fits
    # in [-1/2,1/2]^2, and then translate it by (1/2,1/2) so that
    # it fits in [0,1]^2. While we're at it, we might as well move
    # the z-coordinate to the origin.
    matrixinv = Matrix.diagonal(1/scale, 1/scale, 1).mul(matrixinv)
    matrix = matrix.mul(Matrix.diagonal(scale, scale, 1))
    trans = [-0.5,-0.5,zaverage]

    povprint("  polygon {");
    points = str(n+1)
    for i in range(-1,n):
        pt = vsub(matrixinv.apply(vertices[faces[f][i]]), trans)
        points = points + ", <{:.17g},{:.17g},0>".format(pt[0], pt[1])
    povprint("    {}".format(points))
    povprint("    pigment {{ {} }}".format(pigment))
    povprint("    matrix <")
    for row in matrix.columns():
        povprint("      {:.17g}, {:.17g}, {:.17g},".format(*row))
    povprint("      {:.17g}, {:.17g}, {:.17g}".format(
        *matrix.apply(trans)))
    povprint("    >")
    povprint("  }")
povprint("}")
