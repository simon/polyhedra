#!/usr/bin/env python3

# Transform polyhedron description. That is, we are given an
# orthogonal matrix on the command line (nine arguments, each of which
# is a Python expression), and we transform the polyhedron using that
# matrix.
#
# The matrix is given in column-by-column form. That is, the
# arguments A B C D E F G H I map into the matrix
#
#    ( A D G )
#    ( B E H )
#    ( C F I )
#
# so that (A,B,C) is the image vector of (1,0,0), (D,E,F) that of
# (0,1,0), and (G,H,I) that of (0,0,1).
#
# A further three arguments may be given; if present, they are
# treated as a translation vector.

import sys
import string
import random
import argparse
from math import *

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Apply a matrix transformation to a polyhedron.')
parser.add_argument("matrix", nargs=9, type=eval,
                    help="Entries of a 3x3 transformation matrix to apply "
                    "(column-major).")
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
parser.add_argument("--translate", nargs=3, type=eval,
                    help="Vector to translate by.")
args = parser.parse_args()

matrix = polylib.Matrix.fromcolsflat(3, 3, args.matrix)

def transform(p):
    p = matrix.apply(p)
    if args.translate is not None:
        p = vadd(p, args.translate)
    return p

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, _ = poly.get_dicts()

poly = polylib.Polyhedron()
for name, point in vertices.items():
    poly.vertex(*transform(point), name=name)
for name, vlist in faces.items():
    poly.face(name=name)
    for v in vlist:
        poly.add_to_face(name, v)

with args.outfile() as fh:
    poly.write(fh)
