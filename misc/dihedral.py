#!/usr/bin/env python3

# Matthew Chadwick sent me e-mail about my polyhedra web page, and
# asked a question about dihedral angles of a solid versus cut edge
# angles on a net of that solid. (The cut edge angle at a vertex is
# the plane angle you would get if you cut along _one_ of the edges
# meeting at that vertex and unfolded it so all the faces lay flat:
# the faces would not entirely surround the vertex, and the angle
# they leave over is the cut edge angle at that vertex. It is equal
# to 360 degrees minus the sum of all the interior angles of faces
# meeting at that vertex, and thus is independent of which edge you
# choose to cut along.)
#
# Matthew Chadwick's question was whether the cut edge angle could
# be directly related to the dihedral angles around the vertex.
#
# This program proves the answer is no. It constructs, by 3D
# geometry and a rather nasty binary search, a vertex at which five
# faces meet in such a way that all five dihedral angles between
# them are exactly the same as the dihedral angle of the
# icosahedron. However, the faces' interior angles are not all the
# same, and in particular they sum to more than 300 degrees so that
# the cut edge angle is not the same as that at an icosahedron
# vertex.
#
# If run with the `-p' option, this program outputs two polyhedron
# description files describing pentagonal pyramids, exhibiting
# precisely the two vertices in question (the regular icosahedron
# vertex and the irregular one with the same dihedral angles).
# Anyone interested might enjoy feeding these to ../drawnet.py and
# actually making the two solids, so as to verify that the dihedral
# angles really are the same in both cases.

import sys
import os
from math import *

# Import polylib from our parent directory
sys.path[1:1] = [os.path.join(sys.path[0], "..")]
import polylib
from polylib import vadd, vsub, vneg, vscale, vdot, vcross, vunit, \
    vnorm, vnorm2, Matrix

def compute_angles(pointlist):
    vs = [vunit(p) for p in pointlist]

    # Compute the face angles: each of these is just derived from
    # the dot product of two adjacent elements of vs.
    faceangles = []
    for i in range(len(vs)):
        v1, v2 = vs[i-1], vs[i]  # 0 -> -1 automatically wraps in Python
        faceangles.append(180/pi * acos(vdot(v1,v2)))

    # Now the cut edge angle is 360 minus the sum of those angles.
    cutangle = 360
    for a in faceangles:
        cutangle = cutangle - a

    # Next, compute each dihedral angle. We do this by taking a set
    # of three adjacent vectors in the list and doing a projection
    # so as to remove any component along the middle one, so that
    # the outer two end up projected into a plane perpendicular to
    # the middle one. Then we measure the angle between them as
    # before.
    dihedrals = []
    for i in range(len(vs)):
        v1, v2, v3 = vs[i-2], vs[i-1], vs[i]  # wrapping happens again

        v1 = vsub(v1, vscale(v2, vdot(v1,v2)))
        v3 = vsub(v3, vscale(v2, vdot(v3,v2)))

        dihedrals.append(180/pi * acos(vdot(v1,v3) / (vnorm(v1)*vnorm(v3))))

    return [faceangles, cutangle, dihedrals]

def display_angles(pointlist, names, filename):
    faceangles, cutangle, dihedrals = compute_angles(pointlist)

    # If given the `-p' command-line option, we output a polyhedron
    # description instead of this raw display.
    if len(sys.argv) > 1 and sys.argv[1] == "-p":
        # First, find a plausible vertical axis for the polyhedron,
        # by normalising the vectors and averaging the result.
        vsum = (0,0,0)
        for v in pointlist:
            vsum = vadd(vsum, vunit(v))
        vsum = vunit(vsum)

        # Next, transform each edge vector so that its component in
        # the direction of vsum is the same.
        newpoints = []
        for v in pointlist:
            component = vdot(v, vsum)
            v = vscale(v, 1.0/component)
            newpoints.append(v)

        # Now find a plausible centre point for the solid, by
        # adding up all those points and dividing by n+1 (so we
        # also `average' in the origin).
        centroid = (0,0,0)
        for v in newpoints:
            centroid = vadd(v, centroid)
        centroid = vscale(v, 1.0 / (1+len(pointlist)))

        poly = polylib.Polyhedron()

        # Now output the point coordinates.
        poly.vertex(*vneg(centroid), name="apex")
        for i in range(len(newpoints)):
            v = newpoints[i]
            poly.vertex(*vsub(newpoints[i], centroid),
                        name="base{:d}".format(i))

        # Next, write out each face description. First the base.
        poly.face(name="base")
        for i in range(len(newpoints)-1,-1,-1):
            poly.add_to_face("base", "base{:d}".format(i))

        for i in range(len(newpoints)):
            j = (i+1) % len(newpoints)
            facename = "face{:d}with{:d}".format(i,j)
            poly.face(name=facename)
            poly.add_to_face(facename, "apex")
            poly.add_to_face(facename, "base{:d}".format(i))
            poly.add_to_face(facename, "base{:d}".format(j))

        with open(filename, "w") as fh:
            poly.write(fh)

    else:
        for i in range(len(names)):
            sys.stdout.write("{} = {:f} {:f} {:f}\n".format(
                names[i], *pointlist[i]))
        for i in range(len(names)):
            sys.stdout.write("Face angle {v}O{w} = {a:f}\n".format(
                v = names[i-1], w = names[i], a = faceangles[i]))
        sys.stdout.write("Cut edge angle = {:f}\n".format(cutangle))
        for i in range(len(names)):
            sys.stdout.write(
                "Dihedral angle of O{u}{v} with O{v}{w} = {a:f}\n".format(
                    u = names[i-2], v = names[i-1], w = names[i],
                    a = dihedrals[i]))

# One vertex of a regular icosahedron, and the five vertices
# surrounding it.

O = [ 0.0, -1.0, -1.6180339887498949 ]
A = vsub([ -1.6180339887498949, 0.0, -1.0 ], O)
B = vsub([ 0.0, 1.0, -1.6180339887498949 ], O)
C = vsub([ 1.6180339887498949, 0.0, -1.0 ], O)
D = vsub([ 1.0, -1.6180339887498949, 0.0 ], O)
E = vsub([ -1.0, -1.6180339887498949, 0.0 ], O)

display_angles([A,B,C,D,E], ["A","B","C","D","E"], "dihedral1")

# Now transform this. We're going to:
#  - keep vector B fixed
#  - rotate A by x degrees towards B, keeping it in the plane OAB
#  - rotate C by x degrees towards B, keeping it in the plane OCB
#  - (thus preserving the dihedral angle between OAB and OCB)
#  - rotate E exactly as we rotated A (preserving the dihedral
#    between OAB and OAE)
#  - rotate D exactly as we rotated C (preserving the dihedral
#    between OCB and OCD)

angle1 = pi/20

# Determine the axis about which to rotate A and E (it's the cross
# product of A and B), and construct a rotation matrix about that axis.
AEmatrix = Matrix.rotate_about(vcross(B,A), -angle1)
# And rotate A and E via that matrix.
A = AEmatrix.apply(A)
E = AEmatrix.apply(E)
# Now do the same, rotating C and D about an axis which is the
# cross product of C and B.
CDmatrix = Matrix.rotate_about(vcross(C,B), angle1)
C = CDmatrix.apply(C)
D = CDmatrix.apply(D)

# Now transform again. This time we're going to:
#  - rotate D away from C keeping it in the plane OCD
#  - rotate E away from A keeping it in the plane OEA
#  - thus frobbing the dihedral angles between the plane ODE and
#    each of the planes OAE and ODC
#  - while preserving the three other dihedrals.
#
# We choose the angle of rotation by binary search so as to end up
# with the same dihedral angles as the original icosahedron.

def make_DE(angle2):
    Dprime = Matrix.rotate_about(vcross(C,D), angle2).apply(D)
    Eprime = Matrix.rotate_about(vcross(A,E), angle2).apply(E)
    return Dprime, Eprime

def attempt(angle2):
    Dprime, Eprime = make_DE(angle2)
    a = compute_angles([A,B,C,Dprime,Eprime])
    return a[2][0] - a[2][1]

bot = pi/6
top = pi/4

assert attempt(top) > 0
assert attempt(bot) < 0

while top - bot > 1e-15:
    mid = (top + bot) / 2
    a = attempt(mid)
    if a > 0:
        top = mid
    else:
        bot = mid

angle2 = (top + bot) / 2

Dprime, Eprime = make_DE(angle2)

display_angles([A,B,C,Dprime,Eprime], ["A'","B'","C'","D'","E'"], "dihedral2")
