#!/usr/bin/env python3

# Read in a polyhedron description, and draw a net in PostScript.

import sys
import string
import collections
import itertools
import functools
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib
from polylib import (vcrosspoint, vintersect, Matrix, vadd, vsub, vscale,
                     vunit, vnorm, vnorm2, vdot, vrot90)

def debug(*a):
    print(*a, file=sys.stderr)

def bbox(points):
    # Return an (xmin,ymin,xmax,ymax) 4-tuple giving the bounding box
    # of the provided list of points.
    #
    # The 'points' array can also contain 4-tuples, which are treated
    # as pairs of concatenated points. This allows you to amalgamate a
    # collection of existing bounding boxes.
    def points_iter():
        for p in points:
            for i in range(0, len(p), 2):
                yield p[i:i+2]
    pi = points_iter()
    x, y = next(pi)
    bbox = [x, y, x, y]
    for x, y in pi:
        bbox[0] = min(bbox[0], x)
        bbox[1] = min(bbox[1], y)
        bbox[2] = max(bbox[2], x)
        bbox[3] = max(bbox[3], y)
    return tuple(bbox)

def vmean(vs):
    # Take the mean of a list or iterable of vectors.
    vs = iter(vs)
    vector_sum, n = next(vs), 1
    for v in vs:
        vector_sum, n = vadd(vector_sum, v), n+1
    return vscale(vector_sum, 1.0 / n)

class Placement(object):
    # Class that stores information about where a given face has been
    # positioned.

    def __init__(self, face, matrix, pos, vertex_list, vertex_locations):
        # Store the face name itself (handy back-reference).
        self.face = face

        # Matrix which rotates the polyhedron so that the x and y
        # components of the face's vertices represent how it is
        # displayed on the plane.
        self.matrix = matrix

        # (x,y) tuple indicating how far the rotated polyhedron is
        # translated to reach its final position.
        self.pos = pos

        # The original vertex names of the face.
        self.vertex_list = vertex_list[:]

        transformed_coords = [self.matrix.apply(vertex_locations[v])
                              for v in vertex_list]

        # Hash mapping vertex names to (x,y) positions.
        self.vpos = { v: vadd(pos, coords[:2])
                      for v, coords in zip(vertex_list, transformed_coords) }

        # The same (x,y) positions stored in vpos, but in a list
        # around the face, in order corresponding to vertex_list.
        self.polygon = [ self.vpos[v] for v in self.vertex_list ]

        # Bounding box on the page, as an (xmin,ymin,xmax,ymax) 4-tuple.
        self.bbox = bbox(self.vpos.values())

        # The z-coordinate of the whole face after transforming by
        # 'matrix'. (Used for projecting pictures on to the
        # polyhedron, but not otherwise.)
        self.height = sum(c[2] for c in transformed_coords) / len(vertex_list)

        # Fields not yet initialised:
        #
        # 'adjacent' will be a hash mapping neighbouring faces to
        # Placement objects describing their potential placements
        # alongside this one. We only fill this in once we place a
        # face for real; it's used as part of the loop deciding which
        # face to place next, and again when we decide on tab shape
        # (because the outline of the face that _would_ go on that
        # edge is a bound on the tab that's going to _actually_ go
        # there).
        #
        # 'vid' will map each vertex name from the input polyhedron's
        # namespace into the id of the corresponding vertex on this
        # face of the net.

    def overlaps(self, rhs, shared_vertices):
        # Decide if two face placements overlap, except if it's
        # because of a vertex that we expect them to share.

        # Quick preliminary check: two faces definitely don't overlap
        # if their bounding boxes are disjoint.
        if (self.bbox[3] < rhs.bbox[1] or self.bbox[1] > rhs.bbox[3] or
            self.bbox[2] < rhs.bbox[0] or self.bbox[0] > rhs.bbox[2]):
            return False

        # If any edge of one polygon intersects an edge of the other,
        # then the polygons overlap.
        for sv1, sv2 in polylib.adjacent_tuples(self.vertex_list):
            if not {sv1, sv2}.isdisjoint(shared_vertices): continue

            sx1, sy1 = self.vpos[sv1]
            sx2, sy2 = self.vpos[sv2]

            for rv1, rv2 in polylib.adjacent_tuples(rhs.vertex_list):
                if not {rv1, rv2}.isdisjoint(shared_vertices): continue

                rx1, ry1 = rhs.vpos[rv1]
                rx2, ry2 = rhs.vpos[rv2]

                if polylib.segments_intersect(sx1, sy1, sx2, sy2,
                                              rx1, ry1, rx2, ry2):
                    return True

        # If the polygons' boundaries are disjoint, then they can only
        # overlap if one is nested entirely inside the other, which we
        # can check by finding a vertex of A (other than the shared
        # ones), and seeing if it's inside B.
        #
        # They could be nested either way round, so we check both ways.

        def vertex_inside(pl, pr):
            vl = next(iter(set(pl.vertex_list).difference(shared_vertices)))
            return polylib.winding_number(pl.vpos[vl], pr.polygon) != 0

        return vertex_inside(self, rhs) or vertex_inside(rhs, self)

class NetDrawer(object):
    def __init__(self, poly):
        self.poly = poly
        self.vertices, self.faces, self.normals = self.poly.get_dicts()

        # Hash that maps face names to Placement objects.
        self.placements = {}

        # Hash mapping each face to a list of its edges, all directed
        # so that the face is on the left of that edge.
        self.faceedges = { face: list(polylib.adjacent_tuples(vlist))
                           for face, vlist in self.faces.items() }

        # Hash mapping each edge to the face on its left.
        self.edgeface = { edge : face
                          for face, elist in self.faceedges.items()
                          for edge in elist }

    def placement_next_to(self, face, v0, v1, prev):
        # Make a Placement object describing where we'd place 'face'
        # if it were to go next to the given placement 'prev' of a
        # neighbouring face. v0,v1 are the vertices of the two faces'
        # shared edge, in the 'prev' direction side.

        # Rotate the face into the right plane.
        matrix = Matrix.rotate_to_z(self.normals[face])

        # Transform the edge vector, and figure out how much to rotate
        # it about the z-axis to bring it into alignment with the same
        # edge on the face we've just placed.
        oldvec = vsub(prev.vpos[v0], prev.vpos[v1])
        newvec = vsub(matrix.apply(self.vertices[v0])[:2],
                      matrix.apply(self.vertices[v1])[:2])
        oldangle = atan2(oldvec[1], oldvec[0])
        newangle = atan2(newvec[1], newvec[0])
        matrix = Matrix.rotate_about_z(oldangle - newangle).mul(matrix)

        # Re-transform the two vertices using the new matrix.
        p0 = matrix.apply(self.vertices[v0])[:2]
        p1 = matrix.apply(self.vertices[v1])[:2]

        # Now figure out where to translate this face to so that this
        # edge is brought into exact conjunction with the
        # corresponding edge on face f. We average this across both
        # vertices, just in case of rounding errors.
        translation = vmean([vsub(prev.vpos[v0], p0),
                             vsub(prev.vpos[v1], p1)])

        return Placement(face, matrix, translation,
                         self.faces[face], self.vertices)

    def place_faces(self, firstface, cmpsign, adjpairs):
        if firstface is None:
            # Pick an arbitrary face to start off with.
            firstface = next(iter(self.faces))

        # Set of possible next faces we could place. Each one is a
        # tuple of (face to be placed, already-placed face to put it
        # next to, vertices of the joining edge).
        placeable = set()

        # Dictionary storing the folded edges.
        #
        # Each key in this dict is a pair of adjacent faces of the
        # polyhedron that are also adjacent in the net. The
        # corresponding value is the pair of polyhedron vertices
        # shared by the two faces.
        self.folded_by_faces = {}

        # The number of faces placed adjacent to each face.
        self.face_neighbours = {f: 0 for f in self.faces}

        # Container object to let place() modify a couple of our variables.
        class container(object): pass
        c = container()

        # Track the set of faces yet to be placed, and the last one we
        # did place.
        unplaced = set(self.faces)
        def place(face, placement, neighbour):
            self.placements[face] = placement
            unplaced.remove(face)
            c.prev_face = face
            c.prev_placement = placement
            if neighbour is not None:
                self.folded_by_faces[face, neighbour] = next(
                    e for e in self.faceedges[face]
                    if (e[1],e[0]) in self.faceedges[neighbour])
                self.face_neighbours[face] += 1
                self.face_neighbours[neighbour] += 1

        # Rotate the first face so that its normal vector points
        # upwards, and position it at the origin.
        place(firstface, Placement(
            firstface, Matrix.rotate_to_z(self.normals[firstface]), (0,0),
            self.faces[firstface], self.vertices), None)

        # Loop round placing the rest of the faces.
        while True:
            # Add entries to 'placeable' for all the possible
            # placements of neighbours of the last face we placed.
            c.prev_placement.adjacent = {}
            for v0, v1 in self.faceedges[c.prev_face]:
                fnext = self.edgeface[v1, v0]
                c.prev_placement.adjacent[fnext] = self.placement_next_to(
                    fnext, v0, v1, c.prev_placement)
                if fnext in unplaced:
                    placeable.add((fnext, c.prev_face, (v0, v1)))

            # Now winnow placeable to remove any other choices for the
            # face we've just placed, and any which overlap it.
            for pl in list(placeable):
                fnext, fprev, shared_vertices = pl
                if (fnext == c.prev_face or
                    c.prev_placement.overlaps(
                        self.placements[fprev].adjacent[fnext],
                        shared_vertices)):
                    placeable.remove(pl)

            # Select one out of the remainder, and go back round the loop.
            # Simplest thing here is to pick the one with the smallest
            # |pos|, to encourage the net to be roughly circular in shape.
            #
            # We also process the adjpairs data here. We tag each possible
            # placement (which itself is a pair of adjacent faces) as Yes,
            # No or Maybe. `Yes' means this face pair is explicitly listed
            # in adjpairs; `No' means this face pair _rules out_ one in
            # adjpairs (i.e. that there is a face already placed which the
            # user wants this one placed next to); `Maybe' means neither of
            # these things is the case. Then we place a Yes pair if
            # possible, a Maybe pair failing that, and if we really have
            # nothing but No pairs left to place then we place a No pair
            # and give a warning.
            bestscore = None
            best = None
            No, Maybe, Yes = range(3)
            for fnext, fprev, _ in placeable:
                brokenrule = None
                if fprev in adjpairs[fnext]:
                    status = Yes
                else:
                    # Go through all the faces which n is requested to be
                    # next to. If any is already placed (and isn't p, but
                    # the above test has ruled that out already), our
                    # status is No, otherwise it's Maybe.
                    status = Maybe
                    for fadj in adjpairs[fnext]:
                        if fadj in self.placements:
                            status = No
                            brokenrule = (fnext, fadj)
                            break

                placement = self.placements[fprev].adjacent[fnext]
                score = (status, vnorm2(placement.pos) * -cmpsign, brokenrule)

                if bestscore is None or score > bestscore:
                    bestscore = score
                    best = fnext, fprev, placement

            if best is None:
                break
            if bestscore[2] is not None:
                debug("!!! unable to place faces", bestscore[2][0], "and",
                      bestscore[2][1], "adjacent");

            fnext, fprev, placement = best
            place(fnext, placement, fprev)

        if len(unplaced) > 0:
            debug("!!!", len(unplaced), "faces still unplaced!")

    def setup_vids(self):
        # At this stage every face has been placed. However, pairs of
        # adjacent faces which share vertices will each contain their own
        # version of the coordinates of those vertices, so now we go
        # through and do a merge pass where we canonicalise all subtly
        # different versions of the coordinates of the same point into
        # _really_ the same thing.
        #
        # To do this, we introduce a new thing called a 'vid': a
        # namespace of identifiers that define points on the 2D net as
        # opposed to points on the polyhedron. We construct a new hash
        # mapping each vid to a canonical pair of coordinates, and
        # each placement structure in self.placements[] acquires a new
        # member `vid' giving the vertex ID of each.

        # There is at most one copy of each polyhedron vertex per
        # face. But when two faces share a folded net edge, their
        # copies of the two vertices at the ends of that edge are
        # identified. So we start by making a graph of those
        # identifications.
        graph = collections.defaultdict(set)
        for (f1, f2), (v1, v2) in self.folded_by_faces.items():
            for v in (v1, v2):
                for fa, fb in [(f1, f2), (f2, f1)]:
                    graph[fa,v].add((fb, v))

        # Assign a vid to each connected component of that graph, by
        # depth-first search. (Including components of size 1, which
        # have no entries in that edge list.)
        vids = itertools.count()
        fv_to_vid = {}
        self.vids = {}
        for face, vlist in self.faces.items():
            for vertex in vlist:
                key = face, vertex
                if key in fv_to_vid:
                    continue # we already got one

                # Assign a vid to this key, and search to find all
                # the other keys that get the same vid.
                vid = next(vids)
                queue = [key]
                pos = 0
                while pos < len(queue):
                    key = queue[pos]
                    pos += 1
                    fv_to_vid[key] = vid
                    for key2 in graph[key]:
                        if key2 not in fv_to_vid:
                            queue.append(key2)

                # Also, while we've got a list of all the versions
                # of this vertex, compute their mean to use as the
                # canonical coordinates for this vid.
                self.vids[vid] = vmean(
                    self.placements[f].vpos[v] for f,v in queue)

        # Set up the 'vid' dictionary for each face placement, and
        # replace the vpos values with the canonical ones we've just
        # computed.
        for f, placement in self.placements.items():
            placement.vid = {v: fv_to_vid[f, v] for v in self.faces[f]}
            placement.vpos = {v: self.vids[vid]
                              for v, vid in placement.vid.items()}

        # Set up folded_by_vids, a set of all pairs of vids that
        # define a fold edge of the net.
        self.folded_by_vids = set()
        for (f1, f2), (v1, v2) in self.folded_by_faces.items():
            self.folded_by_vids.add((fv_to_vid[f1, v1], fv_to_vid[f1, v2]))
            self.folded_by_vids.add((fv_to_vid[f1, v2], fv_to_vid[f1, v1]))

        # And vidconn, the graph on the whole set of vids that
        # indicates which pairs of them are connected together by any
        # line at all of the net.
        self.vidconn = collections.defaultdict(set)
        for placement in self.placements.values():
            vidlist = [placement.vid[v] for v in placement.vertex_list]
            for vid1, vid2 in polylib.adjacent_tuples(vidlist):
                self.vidconn[vid1].add(vid2)
                self.vidconn[vid2].add(vid1)

    def make_outline(self):
        # Put together a single list of cut edges going right round the
        # net.
        graph = collections.defaultdict(list) # maps vid to list of vids
        for face, placement in self.placements.items():
            # Iterate round the edge of each face placement and add
            # all its non-fold edges to our graph.
            for vid1, vid2 in polylib.adjacent_tuples(
                    placement.vid[v] for v in placement.vertex_list):
                if not (vid1,vid2) in self.folded_by_vids:
                    graph[vid1].append(vid2)
                    graph[vid2].append(vid1)

        # We expect the resulting graph to be 2-regular. Transform it
        # into a digraph on its edge orientations, so that if vertex B
        # has neighbours A,C then we link AB -> BC and CB -> CA. That
        # digraph should be 1-regular.
        edgegraph = {}
        for b, (a, c) in graph.items():
            edgegraph[a,b] = b,c
            edgegraph[c,b] = b,a

        # Now it's trivial to walk round an arbitrary direction of
        # that graph, to collect a consistent cycle of edges going in
        # the same direction.
        outline = []
        start_edge = edge = next(iter(edgegraph))
        while True:
            outline.append(edge[0])
            edge = edgegraph[edge]
            if edge == start_edge:
                break

        # And we expect to have collected every vertex of every cut
        # edge, exactly once each.
        assert len(outline) == len(graph)
        assert set(outline) == set(graph)
        return outline

    def choose_tabbed_edges(self):
        # Decide which edges of the net outline will have tabs on
        # them. Create and populate self.tabpos.

        rev = lambda edge: (edge[1],edge[0])

        # Identify all the cut edges, by pairs of polyhedron vertex
        # ids. Each of these is a potential tab position.
        tabpos = set(self.edgeface)
        for e in self.folded_by_faces.values():
            tabpos.discard(e)
            tabpos.discard(rev(e))

        # To decide where to place tabs: first loop over each leaf face, in
        # decreasing order of distance from the centre of the net's
        # approximate bounding box, and try to arrange for it to be tab-free.
        xmin, ymin, xmax, ymax = bbox(
            placement.bbox for placement in self.placements.values())
        centre = vmean([(xmin,ymin), (xmax,ymax)])
        leaflist = [f for f, d in self.face_neighbours.items() if d == 1]
        leaflist.sort(key=lambda f: vnorm2(
            vsub(self.placements[f].pos, centre)))

        for f in leaflist:
            # If all potential tabs on this face still have their
            # other possible location available, make this face
            # tab-free.
            if all(rev(e) in tabpos for e in self.faceedges[f]):
                for e in self.faceedges[f]:
                    tabpos.discard(e)

        # List each tab-pair with both locations still not ruled out.
        undecided = {e for e in tabpos if e < rev(e) and rev(e) in tabpos}

        # And loop over those choosing a place for each one.
        #
        # At the moment we just put the tab closer to the origin in
        # all cases. Perhaps it would be nicer to arrange to not put
        # tabs next to each other if possible, or something along
        # those lines?
        for v1, v2 in undecided:
            p1 = self.placements[self.edgeface[(v1,v2)]]
            p2 = self.placements[self.edgeface[(v2,v1)]]
            e1 = vmean([p1.vpos[v1], p1.vpos[v2]])
            e2 = vmean([p2.vpos[v1], p2.vpos[v2]])
            tabpos.discard((v1,v2) if vnorm2(e1) < vnorm2(e2) else (v2,v1))

        self.tabpos = tabpos

    def choose_tab_shape(self, v1, v2):
        # Decide on the exact shape of the tab on the v1-v2 edge.
        # v1,v2 are polyhedron vertices; our output is to write an
        # entry into self.tabpoints, whose key is the corresponding
        # pair of vids, giving the 2-d coordinates of all the vertices
        # of the tab in between those points on the net.

        # Identify the face the tab will be attached to, and the face
        # it will end up glued to.
        fa, fg = self.edgeface[v1,v2], self.edgeface[v2,v1]
        pa = self.placements[fa]

        # Our original polyhedron description should have been set up
        # with all edges going anticlockwise around the described face.
        # Here's our chance to prove it! Rotate the edge 90 degrees
        # clockwise to produce a vector pointing out of fa into fg.
        parallelvec = vunit(vsub(pa.vpos[v2], pa.vpos[v1]))
        outvec = vrot90(parallelvec)

        # Start by choosing the direction of each edge of the tab
        # incident to an endpoint of our starting polyhedron edge.
        tablines = [self.choose_tab_edge_vector(v1, v2, v) for v in [v1,v2]]

        # Now figure out the shape of the tab. We do this in three
        # stages.
        #
        # Stage 1: simply find the intersection of the two lines, to
        # make the tab a triangle.
        (a1, a2), (b1, b2) = tablines
        # This call to vcrosspoint cannot fail since we have arranged
        # for the tab edges to be non-parallel.
        c1 = vcrosspoint(a1, a2, b1, b2)
        tabvertices = (a1, c1, b1)
        tabheight = vdot(outvec, vsub(c1, a1))

        # Stage 2: shorten the tab into a trapezium if it goes
        # anywhere near the epicentre of the polygon it will be glued
        # to. 'Anywhere near' is defined arbitrarily as more than 4/5
        # of the perpendicular distance from the edge to the
        # epicentre.
        c = vmean(pa.adjacent[fg].vpos.values())
        maxtabheight = vdot(outvec, vsub(c, a1)) * 4 / 5
        if tabheight > maxtabheight:
            # Draw a line segment Pa-Pb parallel to the base edge
            # a1-b1 at the max distance.
            Pa = vadd(a1, vscale(outvec, maxtabheight))
            Pb = vadd(b1, vscale(outvec, maxtabheight))

            # Compute its intersections with the two lines of the
            # triangular tab.
            c1 = vcrosspoint(a1, a2, Pa, Pb)
            c2 = vcrosspoint(b1, b2, Pa, Pb)

            # And make that the new shape of the tab.
            tabvertices = (a1, c1, c2, b1)
            tabheight = maxtabheight

        # Stage 3. At this point we have now successfully finished all
        # the 3D geometry: we know the precise shape we would _like_
        # our tab to be in an ideal world. Now there's just the 2D
        # geometry remaining, i.e. fitting the tab on to the page.
        #
        # So what we do is: we collect together a list of _every_ line
        # segment that we need the tab to avoid. Then we see whether
        # any of the tab lines intersect with them. If so, we find the
        # minimum distance away from the tab baseline at which such an
        # intersection point occurs, and then shorten the tab
        # (possibly a second time) to just below that length.

        lines = set()   # accumulates line segments to worry about

        # Enumerate every line in the actual net that doesn't involve
        # our two starting points.
        pvid1 = pa.vid[v1]
        pvid2 = pa.vid[v2]
        ourvids = {pvid1, pvid2}
        for vid1, vids2 in self.vidconn.items():
            for vid2 in vids2:
                ends = {vid1, vid2}
                if ends.isdisjoint(ourvids):
                    lines.add(tuple(self.vids[vid] for vid in sorted(ends)))

        # Also include the outline of face fg, if it were placed on
        # the page next to fa. This ensures the tab will fit on the
        # face it's being glued on to.
        for v, vv in self.faceedges[fg]:
            if {v,vv}.isdisjoint({v1,v2}):
                lines.add((pa.adjacent[fg].vpos[v], pa.adjacent[fg].vpos[vv]))

        # Finally, include a bisecting edge between every other tabbed
        # edge and this one. This should ensure that any pair of tabs
        # which might otherwise have overlapped one another will be
        # shortened until they don't.
        for v3, v4 in self.tabpos:
            f3 = self.edgeface[(v3,v4)]
            pvid3 = self.placements[f3].vid[v3]
            pvid4 = self.placements[f3].vid[v4]
            if not {pvid3,pvid4}.isdisjoint(ourvids):
                continue
            p1, p2 = self.vids[pvid1], self.vids[pvid2]
            p3, p4 = self.vids[pvid3], self.vids[pvid4]
            d34 = vsub(p4, p3)

            # Only bother with this if the outward vectors from both
            # tab bases face one another.
            problempoints = 0
            for a in p1, p2:
                if vdot(vrot90(d34), vsub(a, p3)) > 0:
                    problempoints += 1
            for b in p3, p4:
                if vdot(vrot90(d34), vsub(b, p1)) > 0:
                    problempoints += 1
            if problempoints < 3:
                continue
            # Figure out which way round to compute the bisecting line.
            if (abs(vdot(vsub(p4, p1), vsub(p2, p1))) <
                abs(vdot(vsub(p3, p1), vsub(p2, p1)))):
                p3, p4 = p4, p3
            if (abs(vdot(vsub(p2, p3), vsub(p4, p3))) <
                abs(vdot(vsub(p1, p3), vsub(p4, p3)))):
                p1, p2 = p2, p1
            p13 = vmean([p1, p3])
            p24 = vsub(vadd(p2, p4), p13)
            lines.add((p13, p24))

        # Now we've got a list of line segments to avoid. Convert it
        # into a list of obstacle points somewhere in the region of
        # our tab.
        #
        # A line segment causes a problem if one of its endpoints is
        # in the region. But it can also cause a problem if both its
        # endpoints are safely outside the region but it cuts
        # _through_ the region in the middle. So we must check the
        # endpoints of each line, and also see if it intersects any
        # edge of our proposed tab outline.

        obstacles = set()

        for lp1, lp2 in lines:
            # Check the endpoints of the line.
            for lp in lp1, lp2:
                dp = vdot(outvec, vsub(lp, a1))
                if not 0 <= dp <= tabheight:
                    continue
                # The point is between the base and the height of
                # the tab. Now see if it lies between the tab side
                # lines.
                dpout = vscale(outvec, dp)
                left = vcrosspoint(a1, a2, vadd(a1, dpout), vadd(b1, dpout))
                right = vcrosspoint(b1, b2, vadd(a1, dpout), vadd(b1, dpout))
                dpleft = vdot(left, parallelvec)
                dpright = vdot(right, parallelvec)
                dpthis = vdot(lp, parallelvec)
                if dpleft < dpthis < dpright:
                    # Intersection.
                    obstacles.add(lp)

            # And check intersections of the line with tab edges.
            for i in range(len(tabvertices)-1):
                t1, t2 = tabvertices[i:i+2]
                if vintersect(t1, t2, lp1, lp2):
                    obstacles.add(vcrosspoint(t1, t2, lp1, lp2))

        if len(obstacles) > 0:
            tabvertices = self.shorten_tab(tuple(tabvertices), obstacles)

        # Done. Store the tab data.
        vid1 = pa.vid[v1]
        vid2 = pa.vid[v2]
        tabvertices = list(tabvertices[1:-1]) # remove the endpoints
        self.tabpoints[(vid1, vid2)] = tuple(tabvertices)
        tabvertices.reverse()
        self.tabpoints[(vid2, vid1)] = tuple(tabvertices)

    def choose_tab_edge_vector(self, v1, v2, v):
        # Subroutine of choose_tab_shape to decide on the initial
        # direction of a tab edge, on the v1-v2 polyhedron edge,
        # incident to vertex v.
        #
        # We make a list of existing lines coming from that point. Add
        # to that the imaginary line that would have appeared if I'd
        # placed face fg adjacent to fa and made this edge into a
        # folded one.
        # 
        # Also, check the adjacent edge in fg and see whether it also
        # has a tab glued on to it; if so, add the imaginary line to
        # the centroid of fg. (This ensures that tabs which are not
        # adjacent in the net, but which glue on to adjacent edges of
        # another face when assembled, do not overlap on that face.)
        # 
        # Then work out which of those many lines limits the angle of
        # the tab end most, trim to 90 degrees if nothing else has
        # already done so (tabs that flare back out _look_ silly!),
        # subtract a little bit for general safety margin, and that's
        # the angle at one end of the tab.

        fa, fg = self.edgeface[v1,v2], self.edgeface[v2,v1]
        pa = self.placements[fa]
        vv = next(iter({v1,v2}-{v}))
        parallelvec = vunit(vsub(pa.vpos[v2], pa.vpos[v1]))
        outvec = vrot90(parallelvec)

        vid, vid2 = pa.vid[v], pa.vid[vv]
        vpos = self.vids[vid]

        # List the coordinates of all the vids v connects to
        # (excluding vv).
        pts = set(self.vids[v] for v in self.vidconn[vid] if v != vid2)

        # Identify the vertex that is v's other neighbour on fg
        # (apart from vv)...
        v3 = next(next(iter(set(vs)-{v,vv}))
                  for vs in polylib.adjacent_tuples(self.faces[fg], 3)
                  if vs[1] == v)
        # ... and add that vertex's location as it would be if we
        # had placed fg next to fa.
        pts.add(pa.adjacent[fg].vpos[v3])

        # See if another tab appears adjacent to this one on fg.
        if v == v1:
            # Our tabpos is (v,vv). Therefore the other tab appears
            # on fg if (v3,v) is also in self.tabpos.
            otheredge = (v3,v)
        else:
            # Our tabpos is (vv,v); so we're looking for (v,v3);
            otheredge = (v,v3)
        if otheredge in self.tabpos:
            # Add the centroid of fg (average of all the vertices) to
            # the points-to-avoid list.
            pts.add(vmean(pa.adjacent[fg].vpos.values()))
        # Finally, we take one more precaution. If any other tabbed
        # edges share a vertex with this one, we include the angle
        # bisector between this edge and that. This ensures that
        # the two tabs will not overlap.
        for v3, v4 in self.tabpos:
            f3 = self.edgeface[(v3,v4)]
            pvid3 = self.placements[f3].vid[v3]
            pvid4 = self.placements[f3].vid[v4]
            pvid1 = pa.vid[v]
            pvid2 = pa.vid[vv]
            if pvid3 == pvid1:
                pass
            elif pvid4 == pvid1:
                pvid3, pvid4 = pvid4, pvid3
            else:
                continue
            if pvid4 == pvid2:
                continue
            p1 = self.vids[pvid2]
            p2 = self.vids[pvid4]
            pts.add(vadd(vpos, vadd(vunit(vsub(p1, vpos)),
                                    vunit(vsub(p2, vpos)))))
        # Now go through the points figuring out which have
        # interesting angles.
        sign = 1 if v == v1 else -1
        bestangle = pi/2
        for p in pts:
            along = sign * vdot(parallelvec, vsub(p, vpos))
            outward = vdot(outvec, vsub(p, vpos))
            angle = atan2(outward, along)
            if 0 < angle < bestangle:
                bestangle = angle
        # Trim bestangle by 10 degrees, or by 1/3 of its size,
        # whichever is smaller.
        if bestangle > pi/6:
            bestangle = bestangle - pi/18
        else:
            bestangle = bestangle * 2 / 3
        v = vadd(vscale(parallelvec, sign * cos(bestangle)),
                 vscale(outvec, sin(bestangle)))
        return vpos, vadd(vpos, v)

    def shorten_tab(self, tabvertices, obstacles):
        # Shorten a tab to avoid it colliding with any point in the
        # set 'obstacles'.
        #
        # We consider three possibilities for reducing the size of
        # the tab.
        #
        #  (a) We cut the tab off horizontally at the height of the
        #      lowest point.
        #
        #  (b) We make the tab's left-hand edge slope more
        #      shallowly, so that it misses all the obstacle
        #      points. This may reduce the tab to a triangle, or it
        #      may leave it as a trapezium.
        #
        #  (c) Exactly as (b), but we alter the right-hand edge.
        #
        # We consider all three possibilities, and pick the one
        # which leaves the tab with the greatest area.

        # Endpoints of the tab.
        a1, b1 = tabvertices[0], tabvertices[-1]
        # The other endpoint of each of the endmost edges of the tab.
        # (These two may be the same point, or not.)
        a2, b2 = tabvertices[1], tabvertices[-2]
        parallel = vsub(b1, a1)
        outvec = vrot90(parallel)
        tabheight = max(vdot(outvec, vsub(p, a1)) for p in tabvertices)

        truncheight = None
        lpoint = rpoint = None
        lmgrad = rmgrad = None

        for p in obstacles:
            # Case (a).
            thisheight = vdot(outvec, vsub(p, a1))
            trimheight = thisheight * 4 / 5 # miss the point by a little
            heightdiff = thisheight - trimheight
            if truncheight == None or truncheight > trimheight:
                truncheight = trimheight
            # Case (b).
            lgrad = thisheight / vdot(vsub(p, a1), vsub(b1, a1))
            if lmgrad == None or lmgrad > lgrad:
                lmgrad = lgrad
                lpoint = vsub(p, vscale(outvec, heightdiff))
            # Case (c).
            rgrad = thisheight / vdot(vsub(p, b1), vsub(a1, b1))
            if rmgrad == None or rmgrad > rgrad:
                rmgrad = rgrad
                rpoint = vsub(p, vscale(outvec, heightdiff))
        assert truncheight != None and lpoint != None and rpoint != None
        # Assemble the new tabvertices array for case (a).
        P1 = vadd(a1, vscale(outvec, truncheight))
        P2 = vadd(b1, vscale(outvec, truncheight))
        c1 = vcrosspoint(a1, a2, P1, P2)
        c2 = vcrosspoint(b1, b2, P1, P2)
        tabvertices_a = (a1, c1, c2, b1)
        # Common code between cases (b) and (c).
        P1 = vadd(a1, vscale(outvec, tabheight))
        P2 = vadd(b1, vscale(outvec, tabheight))
        # Now find the crossing point of the two new tab sides, for
        # each of cases b and c.
        cb = vcrosspoint(a1, lpoint, b1, b2)
        cc = vcrosspoint(a1, a2, b1, rpoint)
        tabvertices_b = [a1, cb, b1]
        tabvertices_c = [a1, cc, b1]
        # For each of these, check whether it's too high, and trim
        # the tab horizontally at its original height if so.
        for tv in tabvertices_b, tabvertices_c:
            c = tv[1]
            dp = vdot(outvec, vsub(c, a1))
            if dp > tabheight:
                c1 = vcrosspoint(a1, c, P1, P2)
                c2 = vcrosspoint(b1, c, P1, P2)
                tv[1:2] = [c1, c2]
        # Now we have all three possibilities; compute the area of
        # each.
        tabvertices = None
        bestarea = None
        for tv in tabvertices_a, tabvertices_b, tabvertices_c:
            area = 0
            for i in range(len(tv)-1):
                c1, c2 = tv[i:i+2]
                h1 = vdot(outvec, vsub(c1, a1))
                h2 = vdot(outvec, vsub(c2, a1))
                w = abs(vdot(parallel, vsub(c2, c1)))
                area = area + w * (h1+h2)/2
            if bestarea == None or area > bestarea:
                bestarea = area
                tabvertices = tv

        assert tabvertices is not None
        return tabvertices

    def compute_net(self, firstface, cmpsign, adjpairs, omit_tabs):
        self.place_faces(firstface, cmpsign, adjpairs)
        self.setup_vids()
        self.outline = self.make_outline()

        self.tabpoints = {}
        if not omit_tabs:
            self.choose_tabbed_edges()
            for v1, v2 in self.tabpos:
                self.choose_tab_shape(v1, v2)
        else:
            self.tabpos = set()

    def output(self, outfile, args):
        # Actually output the net.
        psprint = lambda *a: print(*a, file=outfile)

        psprint("%!PS-Adobe-1.0")
        psprint("%%Pages: 1")
        psprint("%%EndComments")
        psprint("%%Page: 1")

        if args.picture is not None:
            with args.picture() as fh:
                for s in iter(fh.readline, ""):
                    outfile.write(s)

        psprint("gsave")

        # Compute the overall bbox.
        xmin, ymin, xmax, ymax = bbox(
            [placement.bbox[0:2] for placement in self.placements.values()] +
            [placement.bbox[2:4] for placement in self.placements.values()])
        for valuelist in self.tabpoints.values():
            for x, y in valuelist:
                if xmin == None or xmin > x: xmin = x
                if xmax == None or xmax < x: xmax = x
                if ymin == None or ymin > y: ymin = y
                if ymax == None or ymax < y: ymax = y
        # Determine scale factor.
        xscale = 550.0 / (xmax-xmin)
        yscale = 550.0 / (ymax-ymin)
        if xscale < yscale:
            scale = xscale
        else:
            scale = yscale
        scale *= args.scale
        psprint(args.xoffset + 288, args.yoffset + 400, "translate")
        psprint(scale, "dup scale")
        if args.rotate != 0:
            psprint(args.rotate, "rotate")
        # Now centre the bounding box at the origin.
        psprint(-(xmax+xmin)/2, -(ymax+ymin)/2, "translate")
        psprint(args.linewidth / scale, "setlinewidth 1 setlinejoin 1 setlinecap")
        # Draw the picture, if we have one.
        if args.picture is not None:
            for f in self.faces.keys():
                psprint("gsave newpath")
                cmd = "moveto"
                placement = self.placements[f]
                for v in self.faces[f]:
                    vid = placement.vid[v]
                    psprint(self.vids[vid][0], self.vids[vid][1], cmd)
                    cmd = "lineto"
                psprint("closepath clip")
                psprint(placement.pos[0], placement.pos[1], "translate")
                psprint("[")
                for col in placement.matrix.columns():
                    psprint(*col)
                psprint("]", placement.height, "picture")
                psprint("grestore")

        # Draw the cut edges, including tabs.
        psprint("newpath")
        vid0 = self.outline[-1]
        cmd = "moveto"
        for vid in self.outline:
            tp = self.tabpoints.get((vid0,vid), None)
            if tp != None:
                if args.tabcolour is not None:
                    # Shade the tab.
                    psprint("gsave newpath", self.vids[vid0][0], self.vids[vid0][1], "moveto")
                    for x, y in tp:
                        psprint(x, y, "lineto")
                    psprint(self.vids[vid][0], self.vids[vid][1], "lineto")
                    psprint("closepath " + args.tabcolour + " fill grestore")
                for x, y in tp:
                    psprint(x, y, cmd)
                    cmd = "lineto"
            psprint(self.vids[vid][0], self.vids[vid][1], cmd)
            cmd = "lineto"
            vid0 = vid
        psprint("closepath stroke")
        # Draw the folded edges, and the tabbed edges.
        psprint("newpath")
        for vid1, vid2 in self.folded_by_vids:
            if vid1 < vid2: # only draw each fold edge one way round
                psprint(self.vids[vid1][0], self.vids[vid1][1], "moveto")
                psprint(self.vids[vid2][0], self.vids[vid2][1], "lineto")
        for v1, v2 in self.tabpos:
            f = self.edgeface[(v1,v2)]
            vid1 = self.placements[f].vid[v1]
            vid2 = self.placements[f].vid[v2]
            psprint(self.vids[vid1][0], self.vids[vid1][1], "moveto")
            psprint(self.vids[vid2][0], self.vids[vid2][1], "lineto")
        psprint("[", 1 / scale, 4 / scale, "] 0 setdash stroke [] 0 setdash")

        if args.facelabels:
            psprint("/Helvetica findfont", 4/scale, "scalefont setfont")
            for face, placement in self.placements.items():
                sx = sy = sn = 0
                for x, y in placement.vpos.values():
                    sx, sy, sn = sx+x, sy+y, sn+1
                psprint(sx/sn, sy/sn, "moveto ({})".format(face))
                psprint("dup stringwidth pop 2 div neg 0 rmoveto show")

        psprint("showpage grestore")
        psprint("%%EOF")

        outfile.close()

def parse_colour(col):
    if (col[:1] != "#" or len(col) % 3 != 1 or
        col[1:].lstrip(string.hexdigits) != ""):
        raise ValueError("expected colours of the form #rrggbb")
        sys.exit(1)
    col = col[1:]
    n = len(col) / 3
    r = int(col[:n], 16) / (16.0 ** n - 1)
    g = int(col[n:2*n], 16) / (16.0 ** n - 1)
    b = int(col[2*n:3*n], 16) / (16.0 ** n - 1)
    return "{:f} {:f} {:f} setrgbcolor".format(r, g, b)

def parse_adjfacepair(s):
    comma = s.find(",")
    if comma <= 0 or comma == len(s)-1:
        raise ValueError("expected two face names separated by a comma")
    else:
        f1 = s[:comma]
        f2 = s[comma+1:]
        return (f1, f2)

def main():
    opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
    parser = argparse.ArgumentParser(
        description='Draw a PostScript net of a polyhedron.')
    parser.add_argument("infile", nargs="?", type=opener("r"),
                        default=opener("r")("-"),
                        help="Input polyhedron file.")
    parser.add_argument("outfile", nargs="?", type=opener("w"),
                        default=opener("w")("-"),
                        help="File to write output to.")
    parser.add_argument("-c", "--colour", "--color", type=parse_colour,
                        help="Fill colour, in #rrggbb format.")
    parser.add_argument("-s", "--first-face",
                        help="Name of face to start from.")
    parser.add_argument("-S", "--scale", type=float, default=1.0,
                        help="Scale factor.")
    parser.add_argument("-X", "--xoffset", type=float, default=0.0,
                        help="X offset.")
    parser.add_argument("-Y", "--yoffset", type=float, default=0.0,
                        help="y offset.")
    parser.add_argument("-R", "--rotate", type=float, default=0.0,
                        help="Angle to rotate (degrees).")
    parser.add_argument("--linewidth", type=float, default=0.5,
                        help="Thickness of lines.")
    parser.add_argument("-f", "--facelabels", action="store_true",
                        help="Label each face with its name.")
    parser.add_argument("-T", "--notabs", action="store_true",
                        help="Omit tabs.")
    parser.add_argument("--tabcolour", "--tabcolor", type=parse_colour,
                        help="Colour for tabs, in #rrggbb format.")

    # Semi-undocumented options:

    # Attempt to force a pair of faces to be placed adjacent to one
    # another in the net. Expects two face names separated by a comma.
    parser.add_argument(
        "-a", "--adjacent", type=parse_adjfacepair, action="append",
        help="Try to make two (comma-separated) faces adjacent.")
    parser.set_defaults(adjacent=[])

    # Specify a file containing a PostScript fragment. That file is
    # included at the top of the output, and is expected to define a
    # function called `picture'.
    #
    # `picture' will then be called once for each face of the polyhedron,
    # and passed two arguments. The first argument will be an orthogonal
    # matrix (a length-9 array listing the elements of the matrix ordered
    # primarily by columns) and the second a height. When called, the
    # origin and clipping path will be set appropriately for `picture' to
    # transform some sort of spherical graphical description using the
    # given matrix, project it on to the plane (z=height), and draw it.
    # This allows a net to be drawn which folds up to produce a polyhedron
    # covered in a projection of the original spherical image (e.g. an
    # icosahedral globe).
    parser.add_argument("-p", "--picture", type=opener("r"),
                        help="PostScript picture description [see source code "
                        "for more details].")

    # Invert the usual condition for choosing which face to place next and
    # where to put it. The usual condition tries to place faces as close
    # as possible to the centre of the net, in the hope that it will end
    # up more or less circular in shape; inverting that condition
    # encourages nets to be long thin chains of faces.
    parser.add_argument("--linear", action="store_true",
                        help="[EXPERIMENTAL] Encourage long thin nets.")

    args = parser.parse_args()

    if args.colour is None:
        args.colour = "1 setgray"
    if args.tabcolour is None:
        args.tabcolour = "0.9 setgray"

    cmpsign = -1 if args.linear else +1

    adjpairs = collections.defaultdict(set)
    for f1, f2 in args.adjacent:
        adjpairs[f1].add(f2)
        adjpairs[f2].add(f1)

    with args.infile() as fh:
        poly = polylib.Polyhedron.read(fh)

    if args.first_face is not None and not poly.has_face(args.first_face):
        sys.exit("supplied initial face name '{}' does not exist"
                 .format(args.first_face))

    nd = NetDrawer(poly)
    nd.compute_net(args.first_face, cmpsign, adjpairs, args.notabs)
    with args.outfile() as fh:
        nd.output(fh, args)

if __name__ == "__main__":
    main()
