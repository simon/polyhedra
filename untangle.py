#!/usr/bin/env python3

# Convert a polyhedron description into a descriptive game ID for
# `Untangle', a member of my puzzle collection.

import sys
import string
import random
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Convert a polyhedron into an Untangle game id describing '
    'its skeleton.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
args = parser.parse_args()

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, normals = poly.get_dicts()

# Collect the edge list.
edges = []
for i in faces.keys():
    vs = faces[i]
    for k in range(len(vs)):
        v1 = vs[k-1]
        v2 = vs[k]
        if v1 < v2:
            edges.append((v1,v2))
edges.sort()

# Shuffle the vertices.
vertexnumbers = list(range(len(vertices)))
vertexnumber = {}
for i in vertices.keys():
    x = random.choice(vertexnumbers)
    vertexnumber[i] = x
    vertexnumbers.remove(x)

# Make sure at least two edges cross (so the puzzle isn't already
# solved). If not, swap round two vertex numbers so they do.
#
# When points are arranged on a circle, any two lines cross iff the
# endpoints of the two lines alternate on the circle. That is, if
# the circle is rotated so that one endpoint of one line is at
# zero, then the other endpoint of that line is _between_ the
# endpoints of the other line.
nv = len(vertices)
crossing = 0
noncrosses = []
for i in range(len(edges)-1):
    for j in range(i+1, len(edges)):
        i1, i2 = map(lambda x: vertexnumber[x], edges[i])
        j1, j2 = map(lambda x: vertexnumber[x], edges[j])
        if i1 == j1 or i1 == j2 or i2 == j1 or i2 == j2:
            continue
        i2 = (i2 + nv - i1) % nv
        j1 = (j1 + nv - i1) % nv
        j2 = (j2 + nv - i1) % nv
        if (j1 - i2 > 0) != (j2 - i2 > 0):
            crossing = 1
        else:
            noncrosses.append((i, j))
if not crossing:
    i, j = random.choice(noncrosses)
    iv = random.choice(edges[i])
    jv = random.choice(edges[j])
    a, b = vertexnumbers[iv], vertexnumbers[jv]
    vertexnumbers[iv], vertexnumbers[jv] = b, a

edges = [(vertexnumber[t[0]], vertexnumber[t[1]]) for t in edges]
edges = [(min(t[0],t[1]), max(t[0],t[1])) for t in edges]
edges.sort()

output = "{:d}:{}".format(
    len(vertices), ",".join("{:d}-{:d}".format(v1, v2) for v1, v2 in edges))
outfile = args.outfile()
outfile.write(output + "\n")
outfile.close()
