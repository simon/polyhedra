#!/usr/bin/env python3

# Read in a file containing points, and draw a projection of it in
# PostScript.

import sys
import string
import random
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Draw a PostScript diagram of a set of points.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input points file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
args = parser.parse_args()

with polylib.PointReader(args.infile()) as pr:
    points = list(pr)

n = len(points)

outfile = args.outfile()
def psprint(*a):
    print(*a, file=outfile)

psprint("%!PS-Adobe-1.0")
psprint("%%Pages: 1")
psprint("%%EndComments")
psprint("%%Page: 1")
psprint("gsave")
psprint("288 400 translate 150 dup scale 0.0025 setlinewidth")
psprint("newpath 0 0 1 0 360 arc stroke")

s = 0.02
for x, y, z in points:
    if z > 0:
        # X denotes a point at the back.
        psprint("newpath", x+s, y+s, "moveto", x-s, y-s, "lineto")
        psprint(x+s, y-s, "moveto", x-s, y+s, "lineto stroke")
    else:
        # O denotes a point at the front.
        psprint("newpath", x, y, s, "0 360 arc stroke")

psprint("showpage grestore")
psprint("%%EOF")

outfile.close()
