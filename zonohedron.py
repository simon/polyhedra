#!/usr/bin/env python3

import sys
import argparse
import math
import random

import polylib
from polylib import vadd, vsub, vneg, vscale, vdot, vcross, vunit

def convex_hull(points, query):
    """Find the convex hull of a set of points.

    `points` is an iterable collection containing a finite number of
    objects representing points.

    `query` is a callable which must accept any three arguments that
    are distinct points from that collection. If they are collinear,
    it returns 0; otherwise it returns +1 or -1 indicates whether the
    three points in the order given go round the triangle clockwise or
    anticlockwise. (Up to you which one you define to be which.)

    The return value is a list of some or all of the objects from the
    `points` iterable, given in order around the convex hull. For at
    least three points, these are such that any consecutive triple of
    them return (False, +1) from the query function.
    """

    points = iter(points)
    hull = []
    for _ in range(2):
        try:
            hull.append(next(points))
        except StopIteration:
            return hull # trivial case

    # Algorithm taken from 'Axioms and Hulls', by Knuth, section 12.

    a, b = hull
    pred = {a:b, b:a}
    succ = {a:b, b:a}
    inst = {a:0, b:1}
    bp = [a, b]
    ap = [1, 1]

    start = hull[0]
    L = len(bp)

    for p in points:
        # H1
        k = 0

        # H2
        while True:
            if query(bp[k], p, bp[k+1]) > 0:
                k += 1
            if ap[k] < 2:
                break
            k = ap[k]

        if ap[k] == 0:
            continue

        start = p

        # H3
        t = bp[k]
        s = pred[t]
        ap[k] = L
        q = pred[s]
        while q != t and query(p, s, q) > 0:
            ap[inst[s]] = L
            s = q
            q = pred[s]
        q = succ[t]
        while q != s and query(p, q, t) > 0:
            t = q
            ap[inst[t]] = L
            q = succ[t]
        succ[s] = p
        pred[p] = s
        succ[p] = t
        pred[t] = p

        # H4
        bp.extend([p, s,   t, p])
        ap.extend([1, L+2, 1, 0])
        inst[p] = L
        inst[t] = L+2
        L += 4

    hull = [start]
    p = succ[start]
    while p != start:
        hull.append(p)
        p = succ[p]

    return hull

def orientation(a, b, c):
    ax, ay = a
    bx, by = b
    cx, cy = c
    det = (ax*by + bx*cy + cx*ay) - (ay*bx + by*cx + cy*ax)
    return (+1 if det > 0 else -1 if det < 0 else 0)

def facenormal(f):
    return vcross(vsub(f[0][1],f[0][0]), vsub(f[1][1],f[1][0]))

d = lambda *args: print(*args, file=sys.stderr)

def make_poly(vectors):
    # Initialise with a pair of parallelograms back to back.
    origin = (0,0,0)
    v = next(vectors)
    w = next(vectors)
    vw = vadd(v, w)

    allsum = vw

    faces = [[(origin,v), (v,vw), (vw,w), (w,origin)],
             [(origin,w), (w,vw), (vw,v), (v,origin)]]

    # Now absorb the rest of the vectors, one by one.
    for v in vectors:
        vps = list(polylib.unit_normals_to(v))

        points = {p for f in faces for e in f for p in e}
        projections = [(p, tuple(vdot(p, w) for w in vps)) for p in points]
        hull = convex_hull(
            projections, lambda p,q,r: orientation(p[1],q[1],r[1]))
        hull = [h[0] for h in hull]
        for f in faces:
            if vdot(facenormal(f), v) > 0:
                f[:] = [(vadd(a,v), vadd(b,v)) for a,b in f]

        edges = {e for f in faces for e in f}

        for i in range(len(hull)):
            p, q = hull[i], hull[i-1]
            pqi, qpi = (p,q) in edges, (q,p) in edges
            assert (pqi or qpi)
            assert not (pqi and qpi)
            if pqi:
                p,q = q,p
            r, s = vadd(q,v), vadd(p,v)
            faces.append([(p,q), (q,r), (r,s), (s,p)])

        allsum = vadd(allsum, v)

    # Centre the polyhedron on the origin.
    offset = vscale(allsum, -0.5)
    for f in faces:
        f[:] = [tuple(vadd(p, offset) for p in e) for e in f]

    return faces

def make_polyobject(faces):
    vs = list({v for f in faces for e in f for v in e})

    poly = polylib.Polyhedron()

    vname = {v: poly.vertex(*v) for v in vs}
    for f in faces:
        fname = poly.face()
        for e in f:
            poly.add_to_face(fname, vname[e[0]])

    return poly

def read_points(pointreader, perturb=None):
    seen = set()
    for p in pointreader:
        if p not in seen:
            seen.add(p)
            seen.add(vneg(p)) # tolerate p and -p both appearing
            if perturb is not None:
                p = tuple(x + random.uniform(-perturb, perturb) for x in p)
            yield p

def main():
    opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
    parser = argparse.ArgumentParser(description='Make a zonohedron.')
    parser.add_argument("infile", type=opener("r"),
                        nargs="?", default=opener("r")("-"),
                        help="Input file.")
    parser.add_argument("outfile", type=opener("w"),
                        nargs="?", default=opener("w")("-"),
                        help="Output file.")
    parser.add_argument("--perturb", type=float, help="Perturb input vectors "
                        "to avoid edge cases in the flaky algorithm.")
    args = parser.parse_args()

    with polylib.PointReader(args.infile()) as pr:
        vectors = read_points(pr, args.perturb)
        faces = make_poly(vectors)
    poly = make_polyobject(faces)
    with args.outfile() as ofh:
        poly.write(ofh)

if __name__ == '__main__':
    main()
