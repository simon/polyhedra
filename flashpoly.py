#!/usr/bin/env python3

# Read in a polyhedron description, and output Flash ActionScript
# code suitable for compiling with MTASC (i.e. a .as file) into a
# dedicated viewer application for that polyhedron.

import sys
import os
import string
import random
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Convert a polyhedron into a source file for a Flash viewer.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
args = parser.parse_args()

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, normals = poly.get_dicts()

outfile = args.outfile()
def asprint(*a):
    print(*a, file=outfile)

# Normalise the radius of our polyhedron so that it just fits
# inside the unit sphere.
maxlen = 0
for v in vertices.values():
    vlen = sqrt(v[0]**2 + v[1]**2 + v[2]**2)
    if maxlen < vlen: maxlen = vlen
for v in vertices.keys():
    vv = vertices[v]
    vertices[v] = (vv[0]/maxlen, vv[1]/maxlen, vv[2]/maxlen)

# Assign integer indices to vertices and faces.
vindex = {}
findex = {}
vlist = []
flist = []
for v in vertices.keys():
    vindex[v] = len(vlist)
    vlist.append(v)
for f in faces.keys():
    findex[f] = len(flist)
    flist.append(f)

# We expect the template for the Flash source to be called
# flashpoly.as and to reside in the same directory as this script.
# Python helpfully lets us know that directory at all times.
template = os.path.join(sys.path[0], "flashpoly.as");
tempfile = open(template, "r")
while 1:
    line = tempfile.readline()
    if line == "": break
    if "--- BEGIN POLYHEDRON ---" in line:
        # Instead of the test polyhedron description in the
        # template file, substitute our own.
        for i in range(len(vertices)):
            v = vertices[vlist[i]]
            asprint("       var p{:d} = {{ x:{:.17g}, y:{:.17g},"
                    " z:{:.17g} }};".format(i, v[0], v[1], v[2]))
        for i in range(len(faces)):
            f = faces[flist[i]]
            n = normals[flist[i]]
            fstr = ",".join(["p{:d}".format(vindex[v]) for v in f])
            asprint("       var f{:d} = {{ points: new Array({}), x:{:.17g}, "
                    "y:{:.17g}, z:{:.17g} }};".format(
                        i, fstr, n[0], n[1], n[2]))
        vstr = ",".join(["p{:d}".format(i) for i in range(len(vertices))])
        fstr = ",".join(["f{:d}".format(i) for i in range(len(faces))])
        asprint("       poly = {{ points: new Array({}), "
                "faces: new Array({}) }};".format(vstr, fstr))
        while 1:
            line = tempfile.readline()
            if line == "": break
            if "--- END POLYHEDRON ---" in line: break
    else:
        outfile.write(line)

tempfile.close()
outfile.close()
