#!/usr/bin/env python3

# Read in a polyhedron description, and output a textual description
# of each face including the vertex angles and edge lengths.

import sys
import string
import random
import argparse
from math import pi, acos, sqrt

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Write a textual description of each face of a polyhedron.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
args = parser.parse_args()

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, normals = poly.get_dicts()

outfile = args.outfile()
def oprint(*a):
    print(*a, file=outfile)

# Iterate over each face building a list of ordered pairs of vertices
# (i.e. directed edges) it owns.
diredges = {}
for key, vlist in faces.items():
    for i in range(len(vlist)):
        n0, n1 = [vlist[i+j] for j in range(-1,1)]
        diredges[(n0, n1)] = key

# Iterate over each face and describe it.
for key, vlist in faces.items():
    oprint("Face", key)
    # Iterate over every edge and vertex.
    for i in range(len(vlist)):
        n0, n1, n2 = [vlist[i+j] for j in range(-2,1)]
        v0, v1, v2 = [vertices[vlist[i+j]] for j in range(-2,1)]

        # Use the vectors from v1 to {v0,v2} to determine the interior
        # angle of the face at v1.
        #
        # This isn't rigorous in the case of non-convex polyhedra,
        # because we don't pay attention to the direction of turning.
        # This polyhedron description format has no convention for the
        # direction of traversal of each face, so we'd have to cope
        # with both directions. If this did need doing, then probably
        # the simplest approach would be:
        #
        #  - reduce to two dimensions by transforming the coordinate
        #    system so as to put the face normal along the z-axis
        #  - compute the _exterior_ angle at each vertex _with sign_,
        #    by rotating the vector v0->v1 into the positive x
        #    direction and computing atan2 of the vector v1->v2. (The
        #    usual convention for atan2, that it returns in the
        #    interval [-pi,+pi], is the right one here.)
        #  - add up all the exterior angles. They should sum to either
        #    +2pi or -2pi in total. If the latter, negate them all.
        #  - now convert every exterior angle into an interior one by
        #    subtracting from pi, at which point the negative exterior
        #    angles (representing concave corners) translate into
        #    interior angles greater than pi.
        #
        # As a byproduct, that analysis also tells us the direction of
        # traversal of each face.

        v1v0 = tuple([v0[i]-v1[i] for i in range(3)])
        v1v2 = tuple([v2[i]-v1[i] for i in range(3)])
        sqrlen01 = sum([v1v0[i]**2 for i in range(3)])
        sqrlen12 = sum([v1v2[i]**2 for i in range(3)])
        dotprod = sum([v1v0[i]*v1v2[i] for i in range(3)])
        cosine = dotprod / sqrt(sqrlen01 * sqrlen12)
        angle = acos(cosine) * 180/pi
        oprint("    Angle at vertex", n1, "=", angle, "degrees")

        # Also give the length of the edge leading to the next vertex.
        length = sqrt(sqrlen12)
        oprint("    Length of edge", n1, "-", n2, "=", length)

        # And the dihedral angle between that and the adjacent face.
        otherface = diredges[(n2, n1)]
        thisnormal = normals[key]
        thatnormal = normals[otherface]
        # Be cautious, just in case due to some annoying bug the
        # normals aren't actually normalised.
        sqrlenthis = sum([thisnormal[i]**2 for i in range(3)])
        sqrlenthat = sum([thatnormal[i]**2 for i in range(3)])
        dotprod = sum([thisnormal[i]*thatnormal[i] for i in range(3)])
        cosine = dotprod / sqrt(sqrlenthis * sqrlenthat)
        # Dihedral angles seem to be typically quoted not as the angle
        # between normal vectors but as 180 minus that, i.e. the angle
        # as measured _inside_ the solid. For example, the
        # dodecahedron's dihedral angle is generally quoted as 116.6
        # degrees, not 63.4.
        dihedral = acos(-cosine) * 180/pi
        oprint("    Dihedral angle with face", otherface, "=", dihedral,
               "degrees")

outfile.close()
