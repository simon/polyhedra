#!/usr/bin/env python3

# Sanitise a polyhedron description, by merging vertices and faces
# which `obviously' ought to be merged.

import sys
import string
import random
import argparse
from math import pi, asin, atan2, cos, sin, sqrt

import polylib

opener = lambda mode: lambda fname: lambda: argparse.FileType(mode)(fname)
parser = argparse.ArgumentParser(
    description='Clean up a polyhedron by merging (near-)coplanar faces.')
parser.add_argument("infile", nargs="?", type=opener("r"),
                    default=opener("r")("-"), help="Input polyhedron file.")
parser.add_argument("outfile", nargs="?", type=opener("w"),
                    default=opener("w")("-"),
                    help="File to write output to.")
parser.add_argument("-v", "--verbose", action="store_true",
                    help="Report details of changes made.")
args = parser.parse_args()

with args.infile() as fh:
    poly = polylib.Polyhedron.read(fh)
vertices, faces, normals = poly.get_dicts()

global_done_something = 0

# Constants.
vthreshold = 1e-3    # two vertices closer than this are considered identical
nthreshold = 1e-3    # two normals closer than this are considered parallel

if args.verbose:
    def report(*args):
        print(*args, file=sys.stderr)
else:
    def report(*args):
        pass

# Merge vertices.
vweight = {}
while 1:
    done_something = 0
    for i in vertices.keys():
        xi, yi, zi = vertices[i]
        for j in vertices.keys():
            xj, yj, zj = vertices[j]
            if i == j:
                continue
            d = sqrt((xj - xi) ** 2 + (yj - yi) ** 2 + (zj - zi) ** 2)
            if d < vthreshold:
                # Merge vertices i and j. This means
                #  - we replace them with a single vertex which has
                #    their mean position. (The mean is weighted by
                #    the number of other vertices we have already
                #    merged into them, so that merging five
                #    vertices always ends up with the _unweighted_
                #    mean of the original five.)
                #  - we replace every reference to the two original
                #    vertices with the new one. In particular, any
                #    face on whose boundary the two vertices are
                #    adjacent has them both replaced with a single
                #    occurrence of the new one.
                #  - surface normals are left alone. It's simplest.
                newname = "merged_" + i + "_" + j
                while vertices.has_key(newname):
                    newname = "x" + newname
                wi = vweight.get(i, 1)
                wj = vweight.get(j, 1)
                xn = (xi * wi + xj * wj) / (wi + wj)
                yn = (yi * wi + yj * wj) / (wi + wj)
                zn = (zi * wi + zj * wj) / (wi + wj)
                vertices[newname] = (xn, yn, zn)
                vweight[newname] = wi + wj
                for f in faces.keys():
                    flist = faces[f]
                    for k in range(len(flist)):
                        if flist[k] == i or flist[k] == j:
                            flist[k] = newname
                    for k in range(len(flist)):
                        if flist[k] == newname and flist[k-1] == newname:
                            del flist[k]
                            break
                del vertices[i]
                del vertices[j]
                report("Merged vertices: {}, {} -> {}".format(i, j, newname))
                done_something = global_done_something = 1
            if done_something:
                break
        if done_something:
            break
    if not done_something:
        break

# Merge faces. We can only do this to a pair of faces sharing an
# edge.
fweight = {}
while 1:
    done_something = 0
    for i in faces.keys():
        for j in faces.keys():
            if j == i: continue
            fi = faces[i]
            fj = faces[j]
            # Find one shared vertex.
            v1 = None
            for v in fi:
                if v in fj:
                    v1 = v
                    break
            if v1 == None:
                continue # no shared vertex at all
            # Now see if a second shared vertex lurks either side
            # of that one.
            vi = fi.index(v1)
            vj = fj.index(v1)
            if fi[(vi+1) % len(fi)] == fj[(vj-1) % len(fj)]:
                v2 = fi[(vi+1) % len(fi)]
                vj = (vj-1) % len(fj)
            elif fi[(vi-1) % len(fi)] == fj[(vj+1) % len(fj)]:
                v2 = v1
                v1 = fi[(vi-1) % len(fi)]
                vi = (vi-1) % len(fi)
            else:
                continue # no pair of shared vertices
            # Now we have a pair of shared vertices: fi[vi] and
            # fi[vi+1] correspond to fj[vj] and fj[vj+1], in the
            # reverse of that order.
            assert fi[vi] == fj[(vj+1) % len(fj)]
            assert fj[vj] == fi[(vi+1) % len(fi)]
            assert v1 == fi[vi]
            assert v2 == fj[vj]

            # Compare the surface normals. We do this by ensuring
            # they are both of unit length, and then simply
            # subtracting them.
            xni, yni, zni = normals[i]
            xnj, ynj, znj = normals[j]
            dni = sqrt(xni**2 + yni**2 + zni**2)
            xni, yni, zni = xni/dni, yni/dni, zni/dni
            dnj = sqrt(xnj**2 + ynj**2 + znj**2)
            xnj, ynj, znj = xnj/dnj, ynj/dnj, znj/dnj
            dn = sqrt((xni-xnj)**2 + (yni-ynj)**2 + (zni-znj)**2)
            if dn < nthreshold:

                # Aha! We have two faces to merge. This means
                #  - our new face consists of the vertex lists of
                #    both other ones, cut at the shared edge and
                #    spliced together. In other words, if one edge
                #    list is [r,s,X,Y,p,q] and the other is
                #    [X,a,b,c,Y], we output [p,q,r,s,X,a,b,c,Y].
                #  - our new face's surface normal is the mean of
                #    the existing two, weighted as before to take
                #    account of how many other faces we've already
                #    merged together.

                newname = "merged_" + i + "_" + j
                while faces.has_key(newname):
                    newname = "x" + newname
                wi = fweight.get(i, 1)
                wj = fweight.get(j, 1)
                xn = (xni * wi + xnj * wj) / (wi + wj)
                yn = (yni * wi + ynj * wj) / (wi + wj)
                zn = (zni * wi + znj * wj) / (wi + wj)
                normals[newname] = (xn, yn, zn)
                fweight[newname] = wi + wj
                if vi == len(fi)-1:
                    # This face is of the form [X,a,b,c,Y].
                    fi = fi[1:-1]
                else:
                    # This face is of the form [r,s,Y,X,p,q].
                    fi = fi[vi+2:] + fi[:vi]
                if vj == len(fj)-1:
                    # This face is of the form [X,a,b,c,Y].
                    fj = fj[1:-1]
                else:
                    # This face is of the form [r,s,Y,X,p,q].
                    fj = fj[vj+2:] + fj[:vj]
                faces[newname] = fi + [v1] + fj + [v2]
                del faces[i]
                del faces[j]
                del normals[i]
                report("Merged faces: {}, {} -> {}".format(i, j, newname))
                done_something = global_done_something = 1
            if done_something:
                break
        if done_something:
            break
    if not done_something:
        break

# With large numbers of merged faces, we might have ended up with
# faces which have an interior vertex <=> faces whose edge lists
# double back on themselves <=> edges both of whose directions are on
# the same face.
facecount = {key:0 for key in vertices}
for fvertices in faces.values():
    for vertex in set(fvertices):
        facecount[vertex] += 1
for key, value in vertices.items():
    if facecount[key] < 2:
        report("Deleted face-interior vertex: {}".format(key))
        del vertices[key]
        done_something = global_done_something = 1
for key, fvertices in faces.items():
    seen = set()
    trimmed = []
    for v in fvertices:
        if v in vertices and v not in seen:
            trimmed.append(v)
            seen.add(v)
    fvertices[:] = trimmed

# Output the cleaned-up polyhedron.
poly = polylib.Polyhedron()
for key, value in vertices.items():
    poly.vertex(*value, name=key)
for key in faces.keys():
    poly.face(name=key)
    for vertex in faces[key]:
        poly.add_to_face(key, vertex)
with args.outfile() as fh:
    poly.write(fh)

# Return success iff we actually did anything.
sys.exit(not global_done_something)
